<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="document-root" content="{{ url('/') }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/dymo.js') }}"></script>
    <script src="https://kit.fontawesome.com/133291f590.js" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @laravelPWA
</head>
<body>
    <div id="app">

        @auth

            <nav class="navbar navbar-expand-md navbar-dark shadow-sm header_master">
                <div class="container-fluid">
                    <a class="navbar-brand logo" href="{{ url('/') }}">
                        <img src="{{ asset('img/logo.png') }}" alt="">
                    </a>

                    @auth

                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <form action="{{ route('search-dates') }}" method="GET" class="d-none d-sm-flex mx-4 w-50 align-items-center">
                        
                        @csrf

                        <input type="text" 
                            name="query" 
                            class="form-control mr-2 border-0" 
                            placeholder="Buscar citas por compañía ó nombre de visitante"
                            required>

                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-search"></i>
                        </button>

                    </form>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Authentication Links -->
                            <li class="nav-item dropdown">
                                <a id="datesDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="far fa-calendar-alt mr-1"></i> 
                                    Citas
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="datesDropdown">
                                    <a class="dropdown-item" href="{{ route('date-type') }}">
                                        Nueva cita
                                    </a>
                                    <a class="dropdown-item" href="{{ route('dates.index') }}">
                                        Citas
                                    </a>
                                    

                                    @role('host')

                                    <a class="dropdown-item" href="{{ route('calendar') }}">
                                        Calendario
                                    </a>

                                    @endrole


                                </div>
                            </li>
        

                            @role('admin')

                            <li class="nav-item dropdown">
                                <a id="visitorsDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-user-friends mr-1"></i> 
                                    Visitantes
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="visitorsDropdown">
                                    
                                    <h6 class="dropdown-header">
                                        <i class="fa fa-users mr-1 text-muted"></i>
                                        Visitantes
                                    </h6>                            
                            
                                    <a class="dropdown-item" href="{{ route('new-visitor') }}">
                                        Nuevo visitante
                                    </a>
                                    <a class="dropdown-item" href="{{ route('visitors') }}">
                                        Visitantes
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="far fa-building mr-1 text-muted"></i>
                                        Compañías
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('companies.create') }}">
                                        Nueva compañía
                                    </a>
                                    <a class="dropdown-item" href="{{ route('companies.index') }}">
                                        Compañías
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-car mr-1 text-muted"></i>
                                        Vehículos
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('new-vehicle') }}">
                                        Nuevo vehiculo
                                    </a>
                                    <a class="dropdown-item" href="{{ route('vehicles') }}">
                                        Vehículos
                                    </a>


                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-truck-moving mr-1 text-muted"></i>
                                        Contenedores
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('new-vehicle') }}">
                                        Nuevo contenedor
                                    </a>
                                    <a class="dropdown-item" href="{{ route('containers.index') }}">
                                        Contenedores
                                    </a>
                                </div>
                            </li>
                            
                            <li class="nav-item dropdown">
                                <a id="departmentsDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-layer-group"></i>
                                    Departamentos
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="departmentsDropdown">

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-layer-group mr-1 text-muted"></i>
                                        Departamentos
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('departments.create') }}">
                                        Nuevo departamento
                                    </a>
                                    <a class="dropdown-item" href="{{ route('departments.index') }}">
                                        Departamentos
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-users mr-1 text-muted"></i>
                                        Empleados
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('new-employee') }}">
                                        Nuevo empleado
                                    </a>
                                    <a class="dropdown-item" href="{{ route('employees') }}">
                                        Empleados
                                    </a>

                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a id="departmentsDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="fa fa-cogs mr-1"></i>
                                    Configuración
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="departmentsDropdown">

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-handshake mr-1 text-muted"></i>
                                        Salas de juntas
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('meeting-rooms.create') }}">
                                        Nueva sala de juntas
                                    </a>

                                    <a class="dropdown-item" href="{{ route('meeting-rooms.index') }}">
                                        Salas de juntas
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-users mr-1 text-muted"></i>
                                        Usuarios
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('users.create') }}">
                                        Nuevo usuario
                                    </a>

                                    <a class="dropdown-item" href="{{ route('users.index') }}">
                                        Usuarios
                                    </a>

                                    <div class="dropdown-divider"></div>

                                    <h6 class="dropdown-header">
                                        <i class="fa fa-cog mr-1 text-muted"></i>
                                        Configuración general
                                    </h6>

                                    <a class="dropdown-item" href="{{ route('config') }}">
                                        Ajustes generales
                                    </a>
                                    
                                </div>
                            </li>

                            @endrole

                            @role('host')
                        
                            <user-notifications user_id="{{ auth()->user()->id }}">
                                
                            </user-notifications>

                            @endrole

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <i class="far fa-user mr-1"></i>
                                    {{ Auth::user()->full_name }} 
                                    <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </div>

                    @endauth
                </div>
            </nav>

        @endauth

        <main class="py-4 main_master">
            
            @auth
                @include('app.components.alerts')
            @endauth

            @yield('content')

        </main> 

        @auth

            <footer class="py-5 border-top bg-white mt-4">
                <div class="container-fluid">
                    
                    @auth
            

                    <barcode-scan></barcode-scan>

                    <advis-notifications></advis-notifications>
        

                    @endauth            
        


                    @php
                        $config = App\Config::first();
                    @endphp

                    <div class="row">
                        <div class="col-sm-6">
                            <p class="lead">{{ $config->name }}</p>
                            @if($config->address)
                            <p>{{ $config->address }}</p>
                            @endif
                            <p class="m-0 text-muted">©{{date('Y')}} {{ config('app.name') }}, Derechos Reservados.</p>
                        </div>
                    </div>

                </div>
            </footer>

        @endauth
    </div>
</body>
</html>
