@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar usuario</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('users.update', $user) }}" method="post" id="updateUserForm">
					
					@method('PATCH')

					@include('app.users.partials.form')

				</form>

			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						<button type="submit" class="btn btn-primary" form="updateUserForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>

						<form action="{{ route('users.destroy', $user) }}" method="post" class="ml-2">
							@csrf
							@method('DELETE')
							<button class="btn btn-outline-danger" onclick="return confirm('¿Estás seguro que deseas eliminar a este usuario?')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop