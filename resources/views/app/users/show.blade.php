@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información general</p>
				<a href="{{ route('users.edit', $user) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Nombre(s):</th>
						<td>{{ $user->first_name }}</td>
					</tr>
					<tr>
						<th>Apellido(s):</th>
						<td>{{ $user->last_name }}</td>
					</tr>
					<tr>
						<th>Correo electrónico:</th>
						<td>{{ $user->email }}</td>
					</tr>
					<tr>
						<th>Role:</th>
						<td>{{ $user->role_name }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

@stop