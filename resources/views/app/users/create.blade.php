@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Nuevo usuario</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('users.store') }}" method="post" id="newUserForm">
					
					@csrf

					@include('app.users.partials.form', ['new' => true])

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newUserForm">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>

@stop