@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre(s):</label>
	<div class="col-sm-6">
		<input type="text" 
			name="first_name" 
			class="form-control @error('first_name') is-invalid @enderror"
			required
			autofocus
			value="{{ isset($user) ? $user->first_name : old('first_name') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Apellido(s):</label>
	<div class="col-sm-6">
		<input type="text" 
			name="last_name" 
			class="form-control @error('last_name') is-invalid @enderror"
			required
			value="{{ isset($user) ? $user->last_name : old('last_name') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Correo electrónico:</label>
	<div class="col-sm-6">
		<input type="email" 
			name="email" 
			class="form-control @error('email') is-invalid @enderror"
			required
			value="{{ isset($user) ? $user->email : old('email') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Role:</label>
	<div class="col-sm-3">
		<select name="role" class="custom-select @error('role') is-invalid @enderror">
			<option value="admin" @isset($user) {{ $user->role == 'admin' ? 'selected' : '' }} @endisset value="admin">
				Administrador
			</option>
			<option value="terminal" @isset($user) {{ $user->role == 'terminal' ? 'selected' : '' }} @endisset value="terminal">
				Terminal
			</option>
			<option value="guard" @isset($user) {{ $user->role == 'guard' ? 'selected' : '' }} @endisset value="guard">
				Caseta
			</option>
		</select>
	</div>
</div>

@empty($new)

<div class="form-group row my-4">
	<div class="col-sm-6 offset-3">
		<p class="lead m-0">
			<i class="fa fa-key mr-2 text-muted"></i>
			Restaurar contraseña
		</p>
	</div>
</div>

@endempty

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		@isset($new)
		<span class="text-danger">*</span>
		@endisset
		Contraseña:
	</label>
	<div class="col-sm-3">
		@empty($new)
			<input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
		@else
			<input type="password" name="password" class="form-control @error('password') is-invalid @enderror" required>
		@endempty
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		@isset($new)
		<span class="text-danger">*</span>
		@endisset
		Confirmar contraseña:
	</label>
	<div class="col-sm-3">
		@empty($new)
			<input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
		@else
			<input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" required>
		@endempty
	</div>
</div>