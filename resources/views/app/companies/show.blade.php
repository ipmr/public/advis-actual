@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información general</p>
				<a href="{{ route('companies.edit', $company) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			
			<table class="table table-striped m-0">
				
				<tbody>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $company->name }}</td>
					</tr>

					<tr>
						<th>Total de visitantes:</th>
						<td>
							<a href="{{ route('companies.visitors.index', $company) }}">
								{{ $company->visitors_total }}
							</a>
						</td>
					</tr>

					<tr>
						<th>Total de citas:</th>
						<td>
							<a href="{{ route('company-dates', $company) }}">
								{{ $company->dates_total }}
							</a>
						</td>
					</tr>

					<tr>
						<th>Total de vehículos</th>
						<td>
							<a href="{{ route('companies.vehicles.index', $company) }}">
								{{ $company->vehicles_total }}
							</a>
						</td>
					</tr>
				</tbody>

			</table>

		</div>

	</div>

@stop