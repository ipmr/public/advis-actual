@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Crear nueva compañía</p>
			</div>
			<div class="card-body">
				
				@if(request('creating-date'))
	
				<form action="{{ route('companies.store') }}?creating-date=true" 
					id="newCompanyForm"
					method="post">

				@else
				
				<form action="{{ route('companies.store') }}" 
					id="newCompanyForm"
					method="post">

				@endif

					@include('app.companies.partials.form')

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newCompanyForm"><i class="fa fa-check mr-2"></i> Guardar</button>
					</div>
				</div>

			</div>
		</div>

	</div>

@stop