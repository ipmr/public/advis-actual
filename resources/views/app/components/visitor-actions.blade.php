@if($date_visitor->status_id == 0)

<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post">
	@csrf
	@method('PATCH')
	<input type="hidden" name="status_id" value="1">
	<button class="nowrap btn btn-outline-primary btn-sm">
		<i class="fa fa-calendar-check mr-1"></i>
		LLegando
	</button>
</form>

@elseif($date_visitor->status_id == 1)

<div class="d-flex align-items-center">
	
	<print-label-btn date_id="{{ $date->id }}" date_visitor_id="{{ $date_visitor->id }}"></print-label-btn>

	<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post" class="w-100 mr-2">
		@csrf
		@method('PATCH')
		<input type="hidden" name="status_id" value="2">
		<button class="nowrap btn btn-outline-primary btn-sm btn-block">
			<i class="fa fa-id-card-alt mr-1"></i>
			Ingresar
		</button>
	</form>

</div>

@elseif($date_visitor->status_id == 2)

<div class="d-flex align-items-center">
	
	<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post" class="w-100 mr-2">
		@csrf
		@method('PATCH')
		<input type="hidden" name="status_id" value="3">
		<button class="nowrap btn btn-outline-primary btn-sm btn-block">
			<i class="fa fa-pause-circle mr-1"></i>
			Pausar
		</button>
	</form>

	<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post" class="w-100">
		@csrf
		@method('PATCH')
		<input type="hidden" name="status_id" value="4">
		<button class="nowrap btn btn-outline-danger btn-sm btn-block" onclick="return confirm('¿Estás seguro que deseas finalizar la actividad del visitante?')">
			<i class="fa fa-hourglass-end mr-1"></i>
			Finalizar
		</button>
	</form>

</div>

@elseif($date_visitor->status_id == 3)

<div class="d-flex align-items-center">
	
	<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post" class="w-100 mr-2">
		@csrf
		@method('PATCH')
		<input type="hidden" name="status_id" value="2">
		<button class="nowrap btn btn-outline-primary btn-sm btn-block">
			<i class="fa fa-play-circle mr-1"></i>
			Continuar
		</button>
	</form>

	<form action="{{ route('dates.visitors.update', [$date, $date_visitor]) }}" method="post" class="w-100">
		@csrf
		@method('PATCH')
		<input type="hidden" name="status_id" value="4">
		<button class="nowrap btn btn-outline-danger btn-sm btn-block" onclick="return confirm('¿Estás seguro que deseas finalizar la actividad del visitante?')">
			<i class="fa fa-hourglass-end mr-1"></i>
			Finalizar
		</button>
	</form>

</div>

@elseif($date_visitor->status_id == 4)

<span class="text-danger nowrap"> Cita finalizada </span>

@endif