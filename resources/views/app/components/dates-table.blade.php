<div class="card shadow-sm">
	<div class="card-header d-flex align-items-center">
		<p class="lead m-0">{{ $title }}</p>

		<a href="{{ isset($route) ? $route : route('dates.create') }}"
			class="btn btn-primary ml-auto">
			<i class="fa fa-plus mr-2"></i>
			Nueva
		</a>
	</div>
	
	@if($dates->total())

	<div class="table-responsive">
		<table class="table table-striped m-0">
			<thead>
				<tr>
					<th>Compañía</th>
					<th>Visitantes</th>
					<th>Asunto</th>
					<th>Departamento</th>
					<th>Fecha y hora</th>
					<th>Vigencia</th>
					<th>Status</th>
					<th>Detalles</th>
				</tr>
			</thead>

			<tbody>
				@foreach($dates as $date)
				<tr>
					<td>
						<a href="{{ route('companies.show', $date->company_id) }}">
							{{ $date->company_name }}
						</a>
					</td>
					<td nowrap>
						<a href="{{ route('dates.show', $date) }}" 
							data-toggle="tooltip" 
							data-html="true" 
							title="
								@foreach($date->visitors as $visitor)
								<p class='m-0'>{{ $visitor->full_name }}</p>
								@endforeach
							">
							
							{{ $date->visitors_total }}

						</a>
					</td>
					<td>{{ $date->subject }}</td>
					<td>{{ $date->department_name }}</td>
					<td nowrap>{{ $date->start }}</td>
					<td nowrap>{{ $date->end }}</td>
					<td>{!! $date->status_badge !!}</td>
					<td nowrap>
						<a href="{{ route('dates.show', $date) }}">Ver más</a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

	@else
	
	<div class="card-body">
		<div class="row">
			<div class="col-sm-4 mx-auto text-center">
				<img src="{{ asset('img/undraw_calendar_dutt.svg') }}" class="svg_img w-50 mb-4" alt="">
				<p class="lead">No se han agregado citas aún.</p>
				<a href="{{ isset($route) ? $route : route('dates.create') }}" class="btn btn-primary">
					<i class="fa fa-plus mr-2"></i> 
					Agrega una
				</a>
			</div>
		</div>
	</div>

	@endif

	@if($dates->total() > $dates->perPage())
	
	<div class="card-footer pb-0">
		
		{{ $dates->links() }}

	</div>

	@endif
</div>