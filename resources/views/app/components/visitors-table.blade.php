<table class="table table-striped m-0">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>E-mail</th>
			<th>Puesto</th>
			<th>Género</th>
			<th>Tipo</th>
			<th>Compañía</th>
			<th>Total de citas</th>
			<th>Detalles</th>
		</tr>
	</thead>

	<tbody>
		@foreach($visitors as $visitor)
		<tr>
			<td>
				<a href="{{ route('companies.visitors.show', [$visitor->company, $visitor]) }}">
					{{ $visitor->full_name }}
				</a>
			</td>
			<td>{{ $visitor->email }}</td>
			<td>{{ $visitor->jobtitle }}</td>
			<td>{{ $visitor->gender }}</td>
			<td>{{ $visitor->type }}</td>
			<td>
				<a href="{{ route('companies.show', $visitor->company) }}">
					{{ $visitor->company_name }}
				</a>
			</td>
			<td>{{ $visitor->dates_total }}</td>
			<td>
				<a href="{{ route('companies.visitors.show', [$visitor->company, $visitor]) }}">
					Ver más
				</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>