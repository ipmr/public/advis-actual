<div class="container-fluid">
	
	@if ($errors->any())
	    <div class="alert alert-danger alert-dismissible fade show" role="alert">
	      
			@foreach ($errors->all() as $error)
			    
			    <p class="m-0">{{ $error }}</p>

			@endforeach

			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				
				<span aria-hidden="true">&times;</span>

			</button>
	    </div>
	@endif


	@if (session('msg'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        
	        {{ session('msg') }}

	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        	
	        	<span aria-hidden="true">&times;</span>

	        </button>

	    </div>
	@endif

	@if (request('new_date'))
	    <div class="alert alert-success alert-dismissible fade show" role="alert">
	        
	        Se ha creado una nueva cita exitosamente.

	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        	
	        	<span aria-hidden="true">&times;</span>

	        </button>

	    </div>
	@endif


	@if (session('info'))
	    <div class="alert alert-info alert-dismissible fade show" role="alert">
	        
	        {{ session('info') }}

	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        	
	        	<span aria-hidden="true">&times;</span>

	        </button>

	    </div>
	@endif

	@if (session('warning'))
	    <div class="alert alert-warning alert-dismissible fade show" role="alert">
	        
	        {{ session('warning') }}

	        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	        	
	        	<span aria-hidden="true">&times;</span>

	        </button>

	    </div>
	@endif
	
</div>
