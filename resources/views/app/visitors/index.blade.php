@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">
					
					{{ $company->name }} | Visitantes

				</p>

				<a href="{{ route('companies.visitors.create', $company) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nuevo
				</a>
			</div>
			
			@if($visitors->count())
				
				@include('app.components.visitors-table')

			@else

				<div class="card-body">
					
					<div class="row">
						<div class="col-sm-4 mx-auto text-center">
							<img src="{{ asset('img/undraw_profile_6l1l.svg') }}" class="svg_img w-50 mb-4" alt="">
							<p class="lead">No se han agregado visitantes aún.</p>
							<a href="{{ route('companies.visitors.create', $company) }}" class="btn btn-primary">
								<i class="fa fa-plus mr-2"></i> 
								Agrega una
							</a>
						</div>
					</div>

				</div>

			@endif
		
			@if($visitors->total() > $visitors->perPage())

			<div class="card-footer pb-0">
				
				{{ $visitors->links() }}

			</div>

			@endif
		</div>

	</div>	

@stop