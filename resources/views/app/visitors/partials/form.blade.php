@csrf

<div class="form-group row">
	<div class="col-sm-7 offset-3">
		
		Campos requeridos marcardos con (<span class="text-danger">*</span>)

	</div>
</div>


@isset($select_company)

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Compañía:</label>
	<div class="col-sm-4">
		
		<select name="company_id" class="custom-select">
			<option selected value="1">Elige una opción</option>
			
			@foreach($companies as $company)
			
			<option value="{{ $company->id }}">{{ $company->name }}</option>

			@endforeach
		</select>

	</div>
</div>


<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<div class="custom-control custom-switch">
			<input type="checkbox" name="independent_visitor" class="custom-control-input" value="1" id="independentCompanySwitch">
			<label class="custom-control-label" for="independentCompanySwitch">
				Visitante independiente
			</label>
		</div>
	</div>
</div>


@endif


<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		<span class="text-danger">*</span>
		Nombre(s):
	</label>
	<div class="col-sm-7">
		<input name="first_name" 
			type="text"
			value="{{ isset($visitor) ? $visitor->first_name : old('first_name') }}" 
			class="form-control @error('first_name') is-invalid @enderror" 
			required>
	</div>
</div>


<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		<span class="text-danger">*</span>
		Apellido(s):
	</label>
	<div class="col-sm-7">
		<input name="last_name" 
			type="text" 
			class="form-control @error('last_name') is-invalid @enderror"
			value="{{ isset($visitor) ? $visitor->last_name : old('last_name') }}"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">E-mail:</label>
	<div class="col-sm-7">
		<input name="email" 
			type="email"
			value="{{ isset($visitor) ? $visitor->email : old('email') }}" 
			class="form-control @error('email') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Puesto laboral:</label>
	<div class="col-sm-7">
		<input name="jobtitle" 
			type="text"
			value="{{ isset($visitor) ? $visitor->jobtitle : old('jobtitle') }}"
			class="form-control @error('jobtitle') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		<span class="text-danger">*</span>
		Género:
	</label>
	<div class="col-sm-4">
		<select name="gender_id" 
			class="custom-select @error('gender') is-invalid @enderror"
			required>
			<option disabled selected value="">Elige una opción</option>
			<option {{ isset($visitor) ? $visitor->gender_id == 0 ? 'selected' : '' : '' }} value="0">Masculino</option>
			<option {{ isset($visitor) ? $visitor->gender_id == 1 ? 'selected' : '' : '' }} value="1">Femenino</option>
		</select>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">
		<span class="text-danger">*</span>
		Tipo:
	</label>
	<div class="col-sm-4">
		<select name="type_id" class="custom-select @error('type_id') is-invalid @enderror">
			
			<option disabled selected value="">Elige una opción</option>

			<option @isset($visitor) {{ $visitor->type_id == 0 ? 'selected' : '' }} @endisset value="0">Visita general</option>
			<option @isset($visitor) {{ $visitor->type_id == 1 ? 'selected' : '' }} @endisset value="1">Vendedor</option>
			<option @isset($visitor) {{ $visitor->type_id == 2 ? 'selected' : '' }} @endisset value="2">Proveedor</option>
			<option @isset($visitor) {{ $visitor->type_id == 3 ? 'selected' : '' }} @endisset value="3">Otro</option>

		</select>
	</div>
</div>