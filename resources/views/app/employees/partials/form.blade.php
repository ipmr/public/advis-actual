<div class="row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

@csrf


@empty($department)
	
	<div class="form-group row">
		<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Departamento:</label>
		<div class="col-sm-4">
			
			<select name="department_id" class="custom-select @error('department_id') is-invalid @enderror" required>
				<option disabled selected value="">Elige una opción</option>
				
				@foreach($departments as $department)
				
					@isset($employee)
						
						<option {{ $employee->department_id == $department->id ? 'selected' : '' }} value="{{ $department->id }}">

					@else
						
						<option value="{{ $department->id }}">

					@endisset			
		

						{{ $department->name }}
					</option>
				@endforeach
			</select>

		</div>
	</div>

@endempty


@isset($employee)

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>ID del empleado:</label>
	<div class="col-sm-4">
		<input type="text" name="employee_id" value="{{ isset($employee) ? $employee->employee_id : old('employee_id') }}" class="form-control @error('employee_id') is-invalid @enderror" required>
	</div>
</div>

@endif

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Status:</label>
	<div class="col-sm-4">
		<select name="status_id" id="" class="custom-select @error('status_id') is-invalid @enderror" required>
			<option disabled selected value="">Elige una opción</option>
			<option {{ isset($employee) ? $employee->status_id == 0 ? 'selected' : '' : '' }} value="0">Operando</option>
			<option {{ isset($employee) ? $employee->status_id == 1 ? 'selected' : '' : '' }} value="1">En periodo de prueba</option>
			<option {{ isset($employee) ? $employee->status_id == 2 ? 'selected' : '' : '' }} value="2">Despedido</option>
			<option {{ isset($employee) ? $employee->status_id == 3 ? 'selected' : '' : '' }} value="3">De baja</option>
			<option {{ isset($employee) ? $employee->status_id == 4 ? 'selected' : '' : '' }} value="4">De vacaciones</option>
			<option {{ isset($employee) ? $employee->status_id == 5 ? 'selected' : '' : '' }} value="5">En incapacidad</option>
			<option {{ isset($employee) ? $employee->status_id == 6 ? 'selected' : '' : '' }} value="6">Practicante</option>
		</select>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre(s):</label>
	<div class="col-sm-6">
		<input type="text" name="first_name" value="{{ isset($employee) ? $employee->first_name : old('first_name') }}" class="form-control @error('first_name') is-invalid @enderror" required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Apellido(s):</label>
	<div class="col-sm-6">
		<input type="text" name="last_name" value="{{ isset($employee) ? $employee->last_name : old('last_name') }}" class="form-control @error('last_name') is-invalid @enderror" required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Correo electrónico:</label>
	<div class="col-sm-6">
		<input type="email" name="email" value="{{ isset($employee) ? $employee->email : old('email') }}" class="form-control @error('email') is-invalid @enderror" required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Puesto laboral:</label>
	<div class="col-sm-6">
		<input type="text" name="jobtitle" value="{{ isset($employee) ? $employee->jobtitle : old('jobtitle') }}" class="form-control @error('jobtitle') is-invalid @enderror" required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Género:</label>
	<div class="col-sm-4">
		<select name="gender_id" class="custom-select @error('gender_id') is-invalid @enderror" id="" required>
			<option disabled selected value="">Elige una opción</option>
			<option {{ isset($employee) ? $employee->gender_id == 0 ? 'selected' : '' : '' }} value="0">Masculino</option>
			<option {{ isset($employee) ? $employee->gender_id == 1 ? 'selected' : '' : '' }} value="1">Femenino</option>
		</select>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Teléfono:</label>
	<div class="col-sm-4">
		<input type="text" name="phone" value="{{ isset($employee) ? $employee->phone : old('phone') }}" class="form-control @error('phone') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Ext:</label>
	<div class="col-sm-2">
		<input type="text" name="ext" value="{{ isset($employee) ? $employee->ext : old('ext') }}" class="form-control @error('ext') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Celular:</label>
	<div class="col-sm-4">
		<input type="text" name="mobile" value="{{ isset($employee) ? $employee->mobile : old('mobile') }}" class="form-control @error('mobile') is-invalid @enderror">
	</div>
</div>

@isset($employee)
	
<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<div class="custom-control custom-switch">
			<input type="checkbox" name="active" @if($employee->active) checked @endif class="custom-control-input" id="activeEmployeeSwitch">
			<label class="custom-control-label" for="activeEmployeeSwitch">Empleado activo</label>
		</div>
	</div>
</div>
	
@endisset