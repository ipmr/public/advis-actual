@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $department->name }} | Empleados</p>
				<a href="{{ route('departments.employees.create', $department) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nuevo
				</a>
			</div>
				
			@if(count($employees))

			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Correo</th>
						<th>Puesto</th>
						<th>Teléfono</th>
						<th>Celular</th>
						<th>Activo</th>
						<th>Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($employees as $employee)
					<tr>
						<td>
							<a href="{{ route('departments.employees.show', [$department, $employee]) }}">{{ $employee->full_name }}</a>
						</td>
						<td>{{ $employee->email }}</td>
						<td>{{ $employee->jobtitle }}</td>
						<td>{{ $employee->phone }} {{ $employee->ext ? ' - (ext. ' . $employee->ext . ')' : '' }}</td>
						<td>{{ $employee->mobile }}</td>
						<td>{{ $employee->is_active }}</td>
						<td nowrap>
							<a href="{{ route('departments.employees.show', [$department, $employee]) }}">
								Ver más
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else
	
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_online_cv_qy9w.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado empleados aún.</p>
						<a href="{{ route('departments.employees.create', $department) }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega uno
						</a>
					</div>
				</div>
			</div>

			@endif
			
			@if($employees->total() > $employees->perPage())
			<div class="card-footer pb-0">
				{{ $employees->links() }}
			</div>
			@endif
		</div>
	</div>

@stop