@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información del empleado</p>

				@if($employee->department)
				<a href="{{ route('departments.employees.edit', [$employee->department, $employee]) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
				@else
				<a href="{{ route('edit-employee', $employee) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
				@endif
			</div>
			
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th>ID del empleado:</th>
						<td>{{ $employee->employee_id }}</td>
					</tr>
					<tr>
						<th>Activo:</th>
						<td>{{ $employee->is_active }}</td>
					</tr>
					<tr>
						<th>Status:</th>
						<td>{{ $employee->status }}</td>
					</tr>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $employee->full_name }}</td>
					</tr>
					<tr>
						<th>Correo:</th>
						<td>{{ $employee->email }}</td>
					</tr>
					<tr>
						<th>Departamento:</th>
						<td>
							@if($employee->department)
							<a href="{{ route('departments.show', $employee->department) }}">
								{{ $employee->department->name }}
							</a>
							@else
							El empleado no esta asignado a un departamento.
							@endif
						</td>
					</tr>
					<tr>
						<th>Puesto laboral:</th>
						<td>{{ $employee->jobtitle }}</td>
					</tr>
					<tr>
						<th>Género:</th>
						<td>{{ $employee->gender }}</td>
					</tr>
					<tr>
						<th>Teléfono:</th>
						<td>{{ $employee->phone }}</td>
					</tr>
					<tr>
						<th>Ext:</th>
						<td>{{ $employee->ext }}</td>
					</tr>
					<tr>
						<th>Celular:</th>
						<td>{{ $employee->mobile }}</td>
					</tr>
				</tbody>
			</table>

		</div>

		<div class="card shadow-sm mt-4">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Historial del empleado</p>
			</div>
			
			@if(count($histories))

			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Evento</th>
						<th>Fecha</th>
					</tr>
				</thead>

				<tbody>
					@foreach($histories as $history)
					<tr>
						<td>{{ $history->description }}</td>
						<td nowrap>{{ $history->date }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else
	
			<div class="card-body">
				El empleado no tiene eventos registrados.
			</div>
	
			@endif

			@if($histories->total() > $histories->perPage())
			<div class="card-footer">
				{{ $histories->links() }}
			</div>
			@endif
		</div>

	</div>

@stop