@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="name" 
			class="form-control @error('name') is-invalid @enderror" 
			required
			value="{{ isset($container) ? $container->name : old('name') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Descripción:</label>
	<div class="col-sm-6">
		<textarea name="description" 
			class="form-control @error('description') is-invalid @enderror" 
			rows="3">{{ isset($container) ? $container->description : old('description') }}</textarea>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Emisor:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="issuing_by" 
			class="form-control @error('issuing_by') is-invalid @enderror"
			value="{{ isset($container) ? $container->issuing_by : old('issuing_by') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">No. de candado:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="padlock" 
			class="form-control @error('padlock') is-invalid @enderror"
			value="{{ isset($container) ? $container->padlock : old('padlock') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">No. de contenedor:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="number" 
			class="form-control @error('number') is-invalid @enderror"
			value="{{ isset($container) ? $container->number : old('number') }}">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">No. de pedimento:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="petition" 
			class="form-control @error('petition') is-invalid @enderror"
			value="{{ isset($container) ? $container->petition : old('petition') }}">
	</div>
</div>