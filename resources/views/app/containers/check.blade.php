@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-sm-6">
				
				<div class="card shadow-sm">
					<div class="card-header d-flex align-items-center">
						<p class="lead m-0">Información general</p>
					</div>
					
					<table class="table table-striped m-0">
						
						<tr>
							<th width="25%">Contenedor:</th>
							<td>{{ $container->name }}</td>
						</tr>

						<tr>
							<th width="25%">Descripción:</th>
							<td>{{ $container->description }}</td>
						</tr>

						<tr>
							<th width="25%">Emitido por:</th>
							<td>{{ $container->issuing_by }}</td>
						</tr>

						<tr>
							<th width="25%">No. de candado:</th>
							<td>{{ $container->padlock }}</td>
						</tr>

						<tr>
							<th width="25%">No. de contenedor:</th>
							<td>{{ $container->number }}</td>
						</tr>

						<tr>
							<th width="25%">No. de pedimento:</th>
							<td>{{ $container->petition }}</td>
						</tr>

					</table>

				</div>

			</div>
		</div>

	</div>

@stop