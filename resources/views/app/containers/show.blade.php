@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información de contenedor</p>
				<a href="{{ route('containers.edit', $container) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			
			<table class="table table-striped m-0">
				<tr>
					<th>Cita:</th>
					<td>
						<a href="{{ route('dates.show', $container->date) }}">
							Ver detalles de la cita
						</a>
					</td>
				</tr>
				<tr>
					<th width="20%">Nombre:</th>
					<td>{{ $container->name }}</td>
				</tr>
				<tr>
					<th>Descripción:</th>
					<td>{{ $container->description }}</td>
				</tr>
				<tr>
					<th>Emisor:</th>
					<td>{{ $container->issuing_by }}</td>
				</tr>

				<tr>
					<th>No. de candado:</th>
					<td>{{ $container->padlock }}</td>
				</tr>

				<tr>
					<th>No. de contenedor:</th>
					<td>{{ $container->number }}</td>
				</tr>

				<tr>
					<th>Pedimento:</th>
					<td>{{ $container->petition }}</td>
				</tr>
			</table>

		</div>

	</div>

@stop
