@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información general</p>
				<a href="{{ route('meeting-rooms.edit', $meeting_room) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $meeting_room->name }}</td>
					</tr>

					<tr>
						<th>Descripción:</th>
						<td>{{ $meeting_room->description }}</td>
					</tr>

					<tr>
						<th>Ubicación:</th>
						<td>{{ $meeting_room->location }}</td>
					</tr>

					<tr>
						<th>Total de citas:</th>
						<td>{{ $meeting_room->dates_total }}</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

@stop