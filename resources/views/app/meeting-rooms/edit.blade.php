@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar sala de juntas</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('meeting-rooms.update', $meeting_room) }}" method="post" id="updateMeetingRoomForm">
					
					@method('PATCH')

					@include('app.meeting-rooms.partials.form')

				</form>

			</div>
			<div class="card-footer">
					
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						
						<button type="submit" form="updateMeetingRoomForm" class="btn btn-primary">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>
	
						<form action="{{ route('meeting-rooms.destroy', $meeting_room) }}" method="post" class="ml-2">
							
							@csrf
							@method('DELETE')

							<button type="submit" 
								class="btn btn-outline-danger"
								onclick="return confirm('¿Estas seguro que deseas eliminar esta sala de juntas?')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>

						</form>

					</div>
				</div>

			</div>
		</div>

	</div>

@stop