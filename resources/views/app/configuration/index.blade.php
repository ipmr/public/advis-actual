@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Configuración general</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('update-config') }}" method="post" id="configForm">
					
					@csrf

					@method('PUT')

					<div class="form-group row">
						<div class="col-sm-6 offset-3">
							<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
						</div>
					</div>
	
					{{--

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Logotipo:</label>
						<div class="col-sm-3">
							<input type="file" name="logo">
						</div>
					</div>

					--}}

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre de la compañía:</label>
						<div class="col-sm-6">
							<input type="text" 
								name="name" 
								class="form-control @error('name') is-invalid @enderror" 
								required
								value="{{ isset($config) ? $config->name : old('name') }}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Dirección:</label>
						<div class="col-sm-6">
							<input type="text" 
								name="address" 
								class="form-control @error('address') is-invalid @enderror" 
								required
								value="{{ isset($config) ? $config->address : old('address') }}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Teléfono:</label>
						<div class="col-sm-3">
							<input type="text" 
								name="phone" 
								class="form-control @error('phone') is-invalid @enderror"
								value="{{ isset($config) ? $config->phone : old('phone') }}">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">RFC:</label>
						<div class="col-sm-3">
							<input type="text" 
								name="rfc" 
								class="form-control @error('rfc') is-invalid @enderror"
								value="{{ isset($config) ? $config->rfc : old('rfc') }}">
						</div>
					</div>

				</form>

			</div>
			<div class="card-footer pb-0">
				<div class="form-group row">
					<div class="col-sm-6 offset-3">
						<button type="submit" form="configForm" class="btn btn-primary">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop