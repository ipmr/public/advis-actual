@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

@empty($vehicle)

	@isset($companies)
		
		<div class="form-group row">
			<label class="col-sm-3 col-form-label text-right">Compañía</label>
			<div class="col-sm-6">
				<select name="company_id" class="custom-select @error('company_id') is-invalid @enderror">
					<option disabled selected value="">Elige una opción</option>
					@foreach($companies as $company)
					<option @isset($vehicle) {{ $vehicle->company_id == $company->id ? 'selected' : '' }} @endisset 
						value="{{ $company->id }}">
						{{ $company->name }}
					</option>
			
				@endforeach
				</select>
			</div>
		</div>
		<div class="form-group row">
			<div class="col-sm-6 offset-3">
				<div class="custom-control custom-switch">
					<input type="checkbox" 
						name="independent_company" 
						class="custom-control-input" 
						id="independentSwitch"
						@isset($vehicle) {{ $vehicle->company_id == 1 ? 'checked' : '' }} @endisset>
					<label class="custom-control-label" for="independentSwitch">Independiente</label>
				</div>
			</div>
		</div>

	@endisset

@else

	<div class="form-group row">
		<div class="col-sm-6 offset-3">
			@if($vehicle->company_id == 1)
			
			<p>Vehículo independiente</p>

			@else
			
			<p>Vehículo de la compañía {{ $vehicle->company_name }}</p>

			@endif
		</div>
	</div>

@endempty

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Tipo:</label>
	<div class="col-sm-6">
		<select class="custom-select @error('type_id') is-invalid @enderror" 
			name="type_id" required>

			<option disabled selected value="">Elige una opción</option>

			<option @isset($vehicle) {{ $vehicle->type_id == 0 ? 'selected' : '' }} @endisset value="0">MICRO</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 1 ? 'selected' : '' }} @endisset value="1">SEDAN</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 2 ? 'selected' : '' }} @endisset value="2">CUV</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 3 ? 'selected' : '' }} @endisset value="3">SUV</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 4 ? 'selected' : '' }} @endisset value="4">HATCHBACK</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 5 ? 'selected' : '' }} @endisset value="5">ROADSTER</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 6 ? 'selected' : '' }} @endisset value="6">PICKUP</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 7 ? 'selected' : '' }} @endisset value="7">VAN</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 8 ? 'selected' : '' }} @endisset value="8">COUPE</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 9 ? 'selected' : '' }} @endisset value="9">SUPER-CAR</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 10 ? 'selected' : '' }} @endisset value="10">CAMPER-VAN</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 11 ? 'selected' : '' }} @endisset value="11">MINI-TRUCK</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 12 ? 'selected' : '' }} @endisset value="12">CABRIOLET</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 13 ? 'selected' : '' }} @endisset value="13">MINI-VAN</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 14 ? 'selected' : '' }} @endisset value="14">TRUCK</option>
			<option @isset($vehicle) {{ $vehicle->type_id == 15 ? 'selected' : '' }} @endisset value="15">BIG-TRUCK</option>
			
		</select>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Marca:</label>
	<div class="col-sm-6">
		<input type="text" 
			value="{{ isset($vehicle) ? $vehicle->brand : old('brand') }}"
			name="brand" 
			class="form-control @error('brand') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Modelo:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="model" 
			value="{{ isset($vehicle) ? $vehicle->model : old('model') }}"
			class="form-control @error('model') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Año:</label>
	<div class="col-sm-3">
		<input type="text" 
			name="year"
			value="{{ isset($vehicle) ? $vehicle->year : old('year') }}"
			class="form-control @error('year') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Placas:</label>
	<div class="col-sm-3">
		<input type="text" 
			name="plates" 
			value="{{ isset($vehicle) ? $vehicle->plates : old('plates') }}"
			class="form-control @error('plates') is-invalid @enderror"
			required>
		<small class="text-muted">
			Las placas solo pueden contener números y letras
		</small>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Color:</label>
	<div class="col-sm-3">
		<input type="text" 
			name="color" 
			value="{{ isset($vehicle) ? $vehicle->color : old('color') }}"
			class="form-control @error('color') is-invalid @enderror"
			required>
	</div>
</div>