@extends('layouts.app')
@section('content')

<div class="container-fluid">

	<div class="row">
		<div class="col-sm-6">
			
			<div class="card shadow-sm mt-4 mt-sm-0">
				<div class="card-header d-flex align-items-center">
					<p class="lead m-0">
						<i class="fa fa-car mr-2 text-muted"></i>
						Recepción de vehículos
					</p>
				</div>
				<form action="{{ route('update-date-vehicle', $date_vehicle) }}" 
					class="table-responsive" 
					enctype="multipart/form-data"
					method="post">

					@csrf

					@method('PUT')

					<table class="table m-0 table-striped">
						<tbody>
							<tr>
								<th>Cita:</th>
								<td>
									@component('app.components.back-btn', [
						
										'route' => route('dates.show', $date),
										'text' => 'Regresar a la cita'
		
									])

									@endcomponent
								</td>
							</tr>
							@available($date->is_available)
							<tr>
								<th>Fotografías:</th>
								<td>
									<input type="file" name="images[]" accept="image/*" multiple>
								</td>
							</tr>
							@endavailable
							<tr>
								<th>Vehículo:</th>
								<td>
									<a href="{{ route('companies.vehicles.show', [$vehicle->company, $vehicle]) }}">
										{{ $vehicle->name }}
									</a>
								</td>
							</tr>
							<tr>
								<th>Total de visitantes:</th>
								<td>
									<a href="{{ route('dates.show', $date) }}">{{ $date_vehicle->date->visitors_total }}</a>
								</td>
							</tr>
							<tr>
								<th>Fecha y hora:</th>
								<td>{{ $date->start }}</td>
							</tr>
							<tr>
								<th>Vigencia:</th>
								<td>{{ $date->end }} | {!! $date->is_expired !!}</td>
							</tr>
							<tr>
								<th>Status:</th>
								<td>
									<div class="d-flex">
										<div class="w-50">
											{!! $date_vehicle->status !!}
										</div>
									</div>
								</td>
							</tr>

							@available($date->is_available)

							<tr>
								<th>Modificar status:</th>
								<td>
									<select name="status_id" id="" class="custom-select">
										<option disabled selected value="">Elige una opción</option>
										<option {{ isset($date_vehicle) ? $date_vehicle->status_id == 0 ? 'selected' : '' : '' }} value="0">En espera</option>
										<option {{ isset($date_vehicle) ? $date_vehicle->status_id == 1 ? 'selected' : '' : '' }} value="1">Dentro de las instalaciones</option>
										<option {{ isset($date_vehicle) ? $date_vehicle->status_id == 2 ? 'selected' : '' : '' }} value="2">Fuera de las instalaciones</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>Comentario:</th>
								<td>
									<textarea name="comment" rows="3" class="form-control @error('comment') is-invalid @enderror" placeholder="Escribe un comentario aquí..."></textarea>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<button type="submit" class="btn btn-primary">
										<i class="fa fa-check mr-2"></i>
										Actualizar
									</button>
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<button type="submit" 
										class="btn btn-link p-0 text-danger"
										onclick="return confirm('¿Estás seguro que deseas remove este vehículo de la cita?')"
										form="deleteVehicleForm">
										<i class="fa fa-trash-alt mr-2"></i>
										Eliminar vehículo de esta cita
									</a>
								</td>
							</tr>

							@endavailable
						</tbody>
					</table>
				</form>


				<form action="{{ route('remove-vehicle-from-date', [$date_vehicle, $date]) }}" 
					method="post"
					id="deleteVehicleForm">
					@csrf
					@method('DELETE')
				</form>
			</div>
	
			<div class="card shadow-sm mt-4">
				<div class="card-header d-flex align-items-center">
					<p class="lead m-0">
						<i class="fa fa-clipboard-list mr-2 text-muted"></i>
						Eventos
					</p>
				</div>
			
				@if(count($date_vehicle->histories))

				<table class="table m-0">
					<thead>
						<tr>
							<th>Actividad</th>
							<th>Fecha</th>
						</tr>
					</thead>

					<tbody>
						@foreach($date_vehicle->histories as $history)
						<tr>
							<td>{{ $history->description }}</td>
							<td>{{ $history->created_at->format('d M, Y h:i a') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				@else
		
				<div class="card-body">
					No se ha registrado ningun evento.
				</div>

				@endif
			</div>

		</div>

		<div class="col-sm-6">
			<div class="card shadow-sm">
				<div class="card-header d-flex align-items-center">
					<p class="lead m-0">
						<i class="fa fa-image mr-2 text-muted"></i>
						Imagenes
					</p>
				</div>
				<div class="card-body">
					
				@if(count($date_vehicle->images))

					<div class="d-flex flex-wrap">
						@foreach($date_vehicle->images as $key => $image)
						<div class="thumb border mr-2" 
							style="background-image: url('{{ asset($image->path)  }}')"
							data-toggle="modal"
							data-target="#img{{ $key }}">
							
							@available($date->is_available)

							<a href="#" 
								class="remove_icon" 
								title="Eliminar imagen"
								onclick="event.preventDefault(); var confirm = window.confirm('¿Estás seguro que deseas eliminar esta imagen?'); if(confirm){document.getElementById('removeVehicle{{ $key }}').submit();}">
								<i class="fa fa-times"></i>
							</a>

							<form id="removeVehicle{{ $key }}" action="{{ route('remove-date-vehicle-image', $image) }}" method="POST" style="display: none;">
							    @csrf
							    @method('DELETE')
							</form>

							@endavailable
							
						</div>


						<div class="modal fade" 
							id="img{{ $key }}" 
							tabindex="-1" 
							role="dialog" 
							aria-labelledby="img{{ $key }}Label" 
							aria-hidden="true">

							<div class="modal-dialog modal-lg" role="document">
								<div class="modal-content">
									<div class="modal-body">
										<img src="{{ asset($image->path) }}" class="img-fluid" alt="">
									</div>
								</div>
							</div>
							
						</div>
						@endforeach
					</div>
				
				@else

					No se han agregado imagenes.

				@endif

				</div> 
			</div>

			<div class="card shadow-sm mt-4">
				<div class="card-header d-flex align-items-center">
					<p class="lead m-0">
						<i class="fa fa-comments mr-2 text-muted"></i>
						Comentarios
					</p>
				</div>
		

				@if(count($date_vehicle->comments))

				<table class="table m-0">
					<thead>
						<tr>
							<th>Comentario</th>
							<th>Autor</th>
							<th>Fecha</th>
						</tr>
					</thead>

					<tbody>
						@foreach($date_vehicle->comments as $comment)
					
						<tr>
							<td>{{ $comment->comment }}</td>
							<td nowrap>{{ $comment->author }}</td>
							<td nowrap>{{ $comment->date }}</td>
						</tr>

						@endforeach
					</tbody>
				</table>

				@else
			
				<div class="card-body">
					No se han agregado comentarios.
				</div>

				@endif
			</div>
		</div>

	</div>

</div>

@stop