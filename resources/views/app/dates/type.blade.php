@extends('layouts.app')

@section('content')
	
	<div class="container">
		<div class="row">
			<div class="col-sm-8 mx-auto">
				<div class="card shadow-sm">
					<div class="card-body">
						
						<p class="lead my-4 text-center">
							Selecciona el tipo de cita que deseas realizar.
						</p>

						<div class="row py-5 d-flex align-items-end">
							<div class="col-sm-6 text-center">
								
								<a href="{{ route('dates.container-date') }}">
									<img src="{{ asset('img/undraw_logistics_x4dc.svg') }}" 
										class="w-50 mb-4" 
										alt="">

									<p class="text-muted lead">
										Cita para <br> contenedores.
									</p>
								</a>

							</div>
							<div class="col-sm-6 text-center border-left">
								
								<a href="{{ route('dates.create') }}">
									<img src="{{ asset('img/undraw_community_8nwl.svg') }}" 
										class="w-50 mb-4" 
										alt="">

									<p class="text-muted lead">
										Cita personal <br> ó por grupo.
									</p>
								</a>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>

@stop