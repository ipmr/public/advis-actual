@extends('layouts.app')
@section('content')
<div class="container-fluid">

	<div class="row">
		<div class="col-sm-6 mb-4 mb-sm-0">
			<div class="card shadow-sm mb-4">
				<div class="card-header d-flex align-items-center">
					
					<p class="lead m-0">
						<i class="fa fa-calendar-alt mr-2 text-muted"></i>
						Información general
					</p>
					
					@role('admin', 'host')
		
						@waiting($date->status_id)
	
							@available($date->is_available)

								<a href="{{ route('dates.edit', $date) }}"
									class="btn btn-primary ml-auto">
									<i class="fa fa-pencil-alt mr-2"></i>
									Editar
								</a>

							@endavailable

						@endwaiting
		
					@endrole

				</div>
				<table class="table table-striped m-0">
					<tbody>
						<tr>
							<th width="25%">Compañía:</th>
							<td>
								<a href="{{ route('companies.show', $date->company) }}">
									{{ $date->company_name }}
								</a>
							</td>
						</tr>
						<tr>
							<th width="25%">Asunto:</th>
							<td>{{ $date->subject }}</td>
						</tr>
						<tr>
							<th width="25%">Descripción:</th>
							<td>{{ $date->description }}</td>
						</tr>
						<tr>
							<th width="25%">Fecha y hora:</th>
							<td>{{ $date->start }}</td>
						</tr>
						<tr>
							<th width="25%">Vigencia:</th>
							<td>{{ $date->end }} | {!! $date->is_expired !!}</td>
						</tr>
						<tr>
							<th width="25%">Departamento:</th>
							<td>
								<a href="{{ route('departments.show', $date->department_id) }}">
									{{ $date->department_name }}
								</a>
							</td>
						</tr>
						<tr>
							<th width="25%">Visitado:</th>
							<td>
								<a href="{{ route('departments.hosts.show', [$date->department_id, $date->host_id]) }}">
									{{ $date->host_name }}
								</a>
							</td>
						</tr>
						<tr>
							<th>Status:</th>
							<td>
								<div class="row">
									<div class="col-sm-3">
										{!! $date->status_badge !!}
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	
			@if($date->histories()->count())

			<div class="card shadow-sm">
				<div class="card-header d-flex align-items-center">
					<p class="lead m-0">
						<i class="fa fa-clipboard-list mr-2 text-muted"></i>
						Eventos
					</p>
				</div>
				<table class="table table-striped m-0">
					<thead>
						<tr>
							<th>Evento</th>
							<th>Fecha</th>
						</tr>
					</thead>

					<tbody>
						@foreach($date->histories as $history)
						<tr>
							<td>{{$history->description}}</td>
							<td>{{$history->date}}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			@endif
		</div>
		<div class="col-sm-6">
			
			<div class="card shadow-sm mb-4">
				<div class="card-header d-flex align-items-center">
					
					<p class="lead m-0">
						<i class="fa fa-users mr-2 text-muted fa-fw"></i>
						Visitantes
					</p>
				
					@role('admin', 'host')

						@waiting($date->status_id)

							@available($date->is_available)
								<a href="{{ route('dates.visitors.index', $date) }}" 
									class="btn btn-primary ml-auto">
									<i class="fa fa-pencil-alt mr-2"></i>
									Editar
								</a>
							@endavailable

						@endwaiting

					@endrole

				</div>
				
				@if($date->visitors->count())

				<div class="table-responsive">
					<table class="table table-striped m-0 table-align-middle">
						<tbody>
							@foreach($date->visitors as $date_visitor)
							<tr>
								<td>

									<div class="d-flex align-items-center">
										
										<div>
											<div class="avatar sm d-inline-block mr-2" style="background-image: url('{{ $date_visitor->avatar }}')"></div>
										</div>

										<a href="{{ route('dates.visitors.show', [$date, $date_visitor]) }}">
											{{ $date_visitor->full_name }}
										</a>

									</div>
									
								</td>
								<td class="text-right">

									@role('admin', 'guard')
									
										@component('app.components.visitor-actions', [
									
											'date' => $date,
											'date_visitor' => $date_visitor
						
										])

										@endcomponent

									@endrole

								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>

				@else
			
				<div class="card-body">
					No hay visitantes agregados a esta cita - <a href="{{ route('dates.visitors.index', $date) }}">Agrega uno aquí</a>
				</div>
			
				@endif


			</div>

			<date-vehicles date_id="{{ $date->id }}" 
				status_id="{{ $date->status_id }}" 
				available="{{ $date->is_available }}">
				
			</date-vehicles>

			<!-- <date-containers date_id="{{ $date->id }}" 
				status_id="{{ $date->status_id }}" 
				available="{{ $date->is_available }}">
					
			</date-containers> -->

			<!-- <date-meeting-room date_id="{{ $date->id }}"></date-meeting-room> -->

			
		</div>
	</div>
</div>
@stop