@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		@component('app.components.dates-table', [
		
			'title' => '('. count($dates) .') Citas relacionadas con ('.$query.')',
			'dates' => $dates
		
		])

		@endcomponent

	</div>

@stop