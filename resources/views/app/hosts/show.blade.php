@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm mb-4">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información general</p>
				<a href="{{ route('departments.hosts.edit', [$department, $host]) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $host->full_name }}</td>
					</tr>

					<tr>
						<th width="20%">Email:</th>
						<td>{{ $host->email }}</td>
					</tr>

					<tr>
						<th width="20%">Género:</th>
						<td>{{ $host->gender }}</td>
					</tr>

					<tr>
						<th width="20%">Puesto laboral:</th>
						<td>{{ $host->jobtitle }}</td>
					</tr>

					<tr>
						<th width="20%">Departamento:</th>
						<td>{{ $host->department_name }}</td>
					</tr>
				</tbody>
			</table>
		</div>

		@include('app.components.dates-table', [
			'dates' => $dates,
			'title' => '('. $dates->total() .') Citas de ' . $host->full_name,
			'route' => route('dates.create') . '?department_id=' . $host->department_id . '&host_id=' . $host->id
		])
	</div>

@stop