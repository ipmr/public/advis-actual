@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre(s):</label>
	<div class="col-sm-6">
		<input type="text" 
			name="first_name" 
			value="{{ isset($host) ? $host->user->first_name : old('first_name') }}"
			class="form-control @error('first_name') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Apellido(s):</label>
	<div class="col-sm-6">
		<input type="text" 
			name="last_name" 
			value="{{ isset($host) ? $host->user->last_name : old('last_name')}}" 
			class="form-control @error('last_name') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Correo electrónico:</label>
	<div class="col-sm-6">
		<input type="email" 
			name="email" 
			value="{{ isset($host) ? $host->email : old('email')}}" 
			class="form-control @error('email') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Género:</label>
	<div class="col-sm-4">
		<select name="gender_id" 
			class="custom-select @error('gender_id') is-invalid @enderror"
			required>
			
			<option disabled selected value="">Elige una opción</option>
			<option @isset($host) {{ $host->employee->gender_id == 0 ? 'selected' : '' }} @endisset value="0">Masculino</option>
			<option @isset($host) {{ $host->employee->gender_id == 1 ? 'selected' : '' }} @endisset value="1">Femenino</option>

		</select>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Puesto laboral:</label>
	<div class="col-sm-4">
		<input type="text" 
			name="jobtitle" 
			value="{{ isset($host) ? $host->employee->jobtitle : old('jobtitle') }}" 
			class="form-control @error('jobtitle') is-invalid @enderror"
			required>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Teléfono:</label>
	<div class="col-sm-4">
		<input type="text" 
			name="phone" 
			value="{{ isset($host) ? $host->employee->phone : old('phone') }}" 
			class="form-control @error('phone') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Ext:</label>
	<div class="col-sm-1">
		<input type="text" 
			name="ext" 
			value="{{ isset($host) ? $host->employee->ext : old('ext') }}" 
			class="form-control @error('ext') is-invalid @enderror">
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Celular:</label>
	<div class="col-sm-4">
		<input type="text" 
			name="mobile" 
			value="{{ isset($host) ? $host->employee->mobile : old('mobile') }}" 
			class="form-control @error('mobile') is-invalid @enderror">
	</div>
</div>


@isset($host)

<div class="form-group row my-4">
	<div class="col-sm-6 offset-3">
		
		<p class="lead m-0">
			<i class="fa fa-key mr-2 text-muted"></i>
			Restaurar contraseña
		</p>

	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Contraseña:</label>
	<div class="col-sm-3">
		<input type="password" name="password" class="form-control @error('password') is-invalid @enderror">
	</div>
</div>


<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Confirmar contraseña:</label>
	<div class="col-sm-3">
		<input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror">
	</div>
</div>

@endisset