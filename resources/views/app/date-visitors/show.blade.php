@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="row">
			<div class="col-sm-6 mx-auto">
				<div class="card shadow-sm">
					<div class="card-header d-flex align-items-center">
						<p class="lead m-0 w-100">
							<i class="fa fa-user mr-2 text-muted"></i>
							Información del visitante
						</p>
					</div>
					<table class="table m-0">
						<tbody>
							<tr>
								<th>Cita:</th>
								<td>
									@component('app.components.back-btn', [
										'route' => route('dates.show', $date_visitor->date),
										'text' => 'Regresar a la cita'
									])
							
									@endcomponent
								</td>
							</tr>
							<tr>
								<th>ID de visita:</th>
								<td>{{ $date_visitor->date_visitor_id }}</td>
							</tr>
							<tr>
								<th width="20%">Fotografía:</th>
								<td>
									<div class="thumb rounded-circle" 
										style="background-image: url('{{ $date_visitor->avatar }}')"
										data-toggle="modal"
										data-target="#showAvatarModal">
											
									</div>

									<div class="modal fade" 
										id="showAvatarModal" 
										tabindex="-1" 
										role="dialog" 
										aria-labelledby="showAvatarModalLabel" 
										aria-hidden="true">

										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-body">
													<img src="{{ $date_visitor->avatar }}" class="img-fluid w-100" alt="">
												</div>
											</div>
										</div>

									</div>
								</td>
							</tr>
							<tr>
								<th>Nombre:</th>
								<td>
									<a href="{{ route('companies.visitors.show', [$date_visitor->visitor->company, $date_visitor->visitor]) }}">
										{{ $date_visitor->full_name }}
									</a>
								</td>
							</tr>
							<tr>
								<th>Status:</th>
								<td>{{ $date_visitor->status }}</td>
							</tr>
							<tr>
								<th>Identificación:</th>
								<td>
									<a href="#" data-toggle="modal" data-target="#showIdModal">Mostrar identificación</a>
									<div class="modal fade" 
										id="showIdModal" 
										tabindex="-1" 
										role="dialog" 
										aria-labelledby="showIdModalLabel" 
										aria-hidden="true">

										<div class="modal-dialog modal-lg" role="document">
											<div class="modal-content">
												<div class="modal-body">
													<img src="{{ $date_visitor->card }}" class="img-fluid w-100" alt="">
												</div>
											</div>
										</div>

									</div>
								</td>
							</tr>
							<tr>
								<th>Fecha y hora:</th>
								<td>{{ $date_visitor->date->start }}</td>
							</tr>
							<tr>
								<th>Vigencia:</th>
								<td>{{ $date_visitor->date->end }} | {!! $date_visitor->date->is_expired !!}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>

	</div>
	
@stop