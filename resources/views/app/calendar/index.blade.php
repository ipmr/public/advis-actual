@extends('layouts.app')

@section('content')

	<div class="container">
		
		<host-calendar user_id="{{ $user->id }}" />

	</div>

@stop