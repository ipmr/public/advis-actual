@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Nuevo departamento</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('departments.store') }}" method="post" id="newDepartmentForm">
					
					@include('app.departments.partials.form')

				</form>

			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newDepartmentForm">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop