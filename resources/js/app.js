require('./bootstrap');

import Vue from 'vue';

import VueBarcodeScanner from 'vue-barcode-scanner';

import DateStart from './components/DateStart';
import NewDate from './components/NewDate';
import PrintLabelBtn from './components/PrintLabelBtn';
import DateVehicles from './components/DateVehicles';
import DateContainers from './components/DateContainers';
import DateMeetingRoom from './components/DateMeetingRoom';
import BarcodeScan from './components/BarcodeScan';
import AdvisNotifications from './components/AdvisNotifications';
import UserNotifications from './components/UserNotifications';
import HostCalendar from './components/HostCalendar';

const app = new Vue({
    el: '#app',
    data: {

    	root: document.head.querySelector('[name="document-root"]').content,
    	token: document.head.querySelector('[name="csrf-token"]').content,
        notifications: false,

    },
    mounted(){
    	$('[data-toggle="tooltip"]').tooltip();

        // this.activate_notifications();

    },
    components: {
        BarcodeScan,
    	DateStart,
    	NewDate,
    	PrintLabelBtn,
        DateVehicles,
        DateContainers,
        DateMeetingRoom,
        AdvisNotifications,
        UserNotifications,
        HostCalendar,
    },

    methods: {

        activate_notifications(){

            let t = this;

            // Comprobamos si el navegador soporta las notificaciones
            if (!("Notification" in window)) {
              console.log("Este navegador no es compatible con las notificaciones de escritorio");
            }

            // Comprobamos si los permisos han sido concedidos anteriormente
            else if (Notification.permission === "granted") {
              // Si es correcto, lanzamos una notificación
              t.notifications = true;
            }

            // Si no, pedimos permiso para la notificación
            else if (Notification.permission !== 'denied' || Notification.permission === "default") {
              Notification.requestPermission(function (permission) {
                // Si el usuario nos lo concede, creamos la notificación
                if (permission === "granted") {

                    t.notifications = true;

                }
              });
            }

            // Por último, si el usuario ha denegado el permiso, y quieres ser respetuoso, no hay necesidad de molestarlo.

        }

    }
});
