<?php

Auth::routes([

	'register' => false

]);

Route::group(['middleware' => 'auth'], function(){

	// DASHBOARD

	Route::redirect('/', 'login');

	Route::get('home', 'HomeController@index')
		->name('home');

	// DATES

	Route::resource('dates', 'DateController');

	Route::get('date-type', 'DateController@type')
		->name('date-type');

	// CITAS PARA CONTENEDORES
	Route::get('container-date', 'DateController@container')->name('dates.container-date');
	
	Route::resource('dates.visitors', 'DateVisitorController');

	Route::get('get-barcode-date', 'DateVisitorController@get_barcode_date')
		->name('get-barcode-date');

	Route::post('update-date-visitor-status-with-barcode', 'DateVisitorController@update_date_visitor_status_with_barcode')
		->name('update-date-visitor-status-with-barcode');

	Route::resource('dates.vehicles', 'DateVehicleController');

	Route::resource('dates.containers', 'DateContainerController');

	Route::get('get-date/{date}', 'DateController@get_date')
		->name('get-date');
	
	Route::post('dates/{date}/set-new-visitor-from-date', 'DateVisitorController@set_new_visitor_from_date')
		->name('set-new-visitor-from-date');

	Route::get('search-dates', 'DateController@search')
		->name('search-dates');

	Route::post('store-pictures/{visitor}', 'DateVisitorController@store_pictures')
		->name('store-pictures');

	Route::post('add-existing-vehicle-to-date/{date}', 'DateController@add_existing_vehicle')
		->name('add-existing-vehicle-to-date');

	Route::post('add-new-vehicle-to-date/{date}', 'DateController@add_new_vehicle')
		->name('add-new-vehicle-to-date');

	Route::delete('remove-vehicle/{date_vehicle}/from-date/{date}', 'DateController@remove_vehicle')
		->name('remove-vehicle-from-date');

	// COMPANIES, VISITORS, VEHICLES & CONTAINERS

	Route::resource('companies', 'CompanyController');
	
	Route::resource('companies.visitors', 'VisitorController');

	Route::get('companies/{company}/dates', 'CompanyController@dates')
		->name('company-dates');

	Route::resource('companies.vehicles', 'VehicleController');

	Route::get('vehicles', 'VehicleController@all')
		->name('vehicles');

	Route::get('new-vehicle', 'VehicleController@new_vehicle')
		->name('new-vehicle');

	Route::post('store-vehicle', 'VehicleController@store_vehicle')
		->name('store-vehicle');

	Route::get('check-vehicle', 'VehicleController@check_vehicle')
		->name('check-vehicle');

	Route::get('search-vehicle-plates', 'VehicleController@search_vehicle_plates')
		->name('search-vehicle-plates');

	Route::put('update-date-vehicle/{date_vehicle}', 'VehicleController@update_date_vehicle')
		->name('update-date-vehicle');

	Route::delete('remove-date-vehicle-image/{image}', 'VehicleController@remove_date_vehicle_image')
		->name('remove-date-vehicle-image');

	Route::resource('containers', 'ContainerController');

	// INDEPENDENT VISITORS

	Route::get('visitors', 'VisitorController@all')->name('visitors');

	Route::get('new-visitor', 'VisitorController@new_visitor')->name('new-visitor');

	Route::post('store-visitor', 'VisitorController@store_visitor')->name('store-visitor');

	// DEPARTMENTS, HOSTS & DEPARTMENT EMPLOYEES

	Route::resource('departments', 'DepartmentController');

	Route::resource('departments.hosts', 'HostController');

	Route::get('user/{user}/host', 'HostController@get_single_host');

	Route::get('user/{user}/host-dates', 'HostController@get_single_host_with_dates');

	Route::resource('departments.employees', 'EmployeeController');

	Route::get('employees', 'EmployeeController@all')
		->name('employees');

	Route::get('new-employee', 'EmployeeController@new_employee')
		->name('new-employee');

	Route::post('store-employee', 'EmployeeController@store_employee')
		->name('store-employee');

	Route::get('show-employee/{employee}', 'EmployeeController@show_employee')
		->name('show-employee');

	Route::get('edit-employee/{employee}', 'EmployeeController@edit_employee')
		->name('edit-employee');

	Route::put('update-employee/{employee}', 'EmployeeController@update_employee')
		->name('update-employee');

	Route::delete('delete-employee/{employee}', 'EmployeeController@destroy_employee')
		->name('destroy-employee');

	Route::get('departments/{department}/dates', 'DepartmentController@dates')
		->name('department-dates');


	// CALENDAR

	Route::get('calendar', 'CalendarController@index')
		->name('calendar');


	// MEETING ROOMS

	Route::resource('meeting-rooms', 'MeetingRoomController');

	// USERS [ADMIN, TERMINAL]

	Route::resource('users', 'UserController');

	Route::get('user/{user}/unread-notifications', 'UserController@unread_notifications')
		->name('get-unread-notifications');

	Route::get('user/{user}/notifications', 'UserController@notifications');

	// GLOBAL CONFIG

	Route::get('config', 'ConfigController@index')->name('config');

	Route::put('update-config', 'ConfigController@update')->name('update-config');

});
