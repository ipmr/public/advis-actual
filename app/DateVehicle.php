<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateVehicle extends Model
{
    

	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [
		'brand',
		'model',
		'year',
		'plates',
		'color',
		'short_name',
		'status'
	];


	public function date(){

		return $this->belongsTo(Date::class, 'date_id');

	}

	public function vehicle(){

		return $this->belongsTo(Vehicle::class, 'vehicle_id');
			
	}

	public function getShortNameAttribute(){

		return $this->vehicle->short_name;

	}


	public function getBrandAttribute(){

		return $this->vehicle->brand;

	}


	public function getModelAttribute(){

		return $this->vehicle->model;

	}

	public function getPlatesAttribute(){

		return $this->vehicle->plates;

	}

	public function getColorAttribute(){

		return $this->vehicle->color;

	}


	public function getYearAttribute(){

		return $this->vehicle->year;

	}

	public function getStatusAttribute(){

		switch ($this->status_id) {
			case 0:
				return '<span class="badge badge-pill w-100 badge-secondary">En espera</span>';
				break;

			case 1:
				return '<span class="badge badge-pill w-100 badge-success">Dentro de instalaciones</span>';
				break;
			
			case 2:
				return '<span class="badge badge-pill w-100 badge-primary">Fuera de instalaciones</span>';
				break;
		}

	}


	public function images(){

		return $this->hasMany(DateVehicleImage::class, 'date_vehicle_id');

	}


	public function comments(){

		return $this->hasMany(DateVehicleComment::class, 'date_vehicle_id');

	}


	public function histories(){

		return $this->hasMany(DateVehicleHistory::class, 'date_vehicle_id');

	}



}
