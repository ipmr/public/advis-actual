<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getFullNameAttribute(){

        return $this->first_name .' '. $this->last_name;

    }


    public function host(){

        return $this->hasOne(Host::class, 'user_id');

    }

    public function getRoleNameAttribute(){

        switch ($this->role) {
            case 'admin':
                return 'Administrador';
                break;

            case 'terminal':
                return 'Terminal';
                break;

            case 'guard':
                return 'Caseta';
                break;

            case 'host':
                return 'Encargado de departamento';
                break;
        }

    }


    public function routeNotificationForNexmo($notification)
    {
        return '+526643864368';
    }

}
