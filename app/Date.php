<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Date extends Model
{

    protected $guarded = [];

    protected $hidden = [
        'status_id',
    ];

    protected $appends = [
        'status',
        'company_name',
        'department_name',
        'host_name',
        'visitors_total',
        'start',
        'end',
    ];

    protected $casts = [

        'start_at' => 'date:d M, Y h:i a',

    ];

    public function histories()
    {

        return $this->hasMany(DateHistory::class, 'date_id');

    }

    public function company()
    {

        return $this->belongsTo(Company::class, 'company_id');

    }

    public function getCompanyNameAttribute()
    {

        return $this->company->name;

    }

    public function visitors()
    {

        return $this->hasMany(DateVisitor::class, 'date_id');

    }

    public function vehicles()
    {

        return $this->hasMany(DateVehicle::class, 'date_id');

    }

    public function getVehiclesTotalAttribute()
    {

        return $this->vehicles()->count();

    }


    public function containers(){

        return $this->hasMany(DateContainer::class)->orderBy('created_at', 'desc');

    }


    public function getVisitorsTotalAttribute()
    {

        return '(' . $this->visitors()->count() . ') Visitantes';

    }

    public function department()
    {

        return $this->belongsTo(Department::class, 'department_id');

    }

    public function getDepartmentNameAttribute()
    {

        return $this->department->name;

    }

    public function host()
    {

        return $this->belongsTo(Host::class, 'host_id');

    }

    public function getHostNameAttribute()
    {

        return $this->host->user->full_name;

    }

    public function getStartAttribute()
    {

        return $this->start_at->format('d M Y - h:i a');

    }

    public function getEndAttribute()
    {

        $start = $this->start_at;

        $vigency = $this->vigency;

        $end = '';

        if ($vigency == 0) {

            $end = Carbon::parse($start)->endOfDay()->format('d M Y - h:i a');

        } else {

            $end = Carbon::parse($start)->addHours($vigency)->format('d M Y - h:i a');

        }

        return $end;

    }

    public function getEndWithoutFormatAttribute()
    {

        $start = $this->start_at;

        $vigency = $this->vigency;

        $end = '';

        if ($vigency == 0) {

            $end = Carbon::parse($start)->endOfDay();

        } else {

            $end = Carbon::parse($start)->addHours($vigency);

        }

        return $end;

    }

    public function getStatusAttribute()
    {

        switch ($this->status_id) {
            case 0:
                return 'En espera';
                break;

            case 1:
                return 'En curso';
                break;

            case 2:
                return 'Finalizada';
                break;

            case 3:
                return 'Cancelada';
                break;
        }

    }

    public function getStatusBadgeAttribute()
    {

        switch ($this->status_id) {
            case 0:
                return '<span class="badge badge-pill w-100 badge-secondary">En espera</span>';
                break;

            case 1:
                return '<span class="badge badge-pill w-100 badge-success">En curso</span>';
                break;

            case 2:
                return '<span class="badge badge-pill w-100 badge-primary">Finalizada</span>';
                break;

            case 3:
                return '<span class="badge badge-pill w-100 badge-danger">Cancelada</span>';
                break;
        }

    }

    public function getIsExpiredAttribute()
    {

        $now = Carbon::now();

        $start = $this->start_at;

        $vigency = $this->vigency;

        $end = '';

        if ($vigency == 0) {

            $end = Carbon::parse($start)->endOfDay();

        } else {

            $end = Carbon::parse($start)->addHours($vigency);

        }

        if ($now->greaterThan($end)) {

            return '<span class="text-danger"><i class="fa fa-times fa-fw fa-sm"></i> Expirada</span>';

        } else {

            return '<span class="text-success"><i class="fa fa-check fa-fw fa-sm"></i> Vigente</span>';

        }

    }

    public function getIsAvailableAttribute()
    {

        $now = Carbon::now();

        $start = $this->start_at;

        $vigency = $this->vigency;

        $end = '';

        if ($vigency == 0) {

            $end = Carbon::parse($start)->endOfDay();

        } else {

            $end = Carbon::parse($start)->addHours($vigency);

        }

        if ($now->greaterThan($end)) {

            return false;

        } else {

            return true;

        }

    }

    public static function format_dates_to_added_to_calendar($dates)
    {

        $formated_dates = [];

        foreach ($dates as $date) {

            info($date->end);

            $item = [

                'title' => $date->subject,
                'start' => Carbon::parse($date->start_at)->format('Y-m-d h:i a'),
                'end'   => Carbon::parse($date->end_without_format)->format('Y-m-d h:i a'),
                'data'  => $date,

            ];

            array_push($formated_dates, $item);

        }

        info($formated_dates);

        return $formated_dates;

    }

}
