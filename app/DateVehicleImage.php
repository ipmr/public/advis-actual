<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateVehicleImage extends Model
{
    

	protected $guarded = [];


	public function vehicle(){

		return $this->belongsTo(DateVehicle::class, 'date_vehicle_id');

	}


	public function getPathAttribute(){


		return asset('vehicles-78xtr45/' . $this->pathname);


	}


}
