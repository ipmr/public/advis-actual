<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::if('role', function ($role1 = null, $role2 = null, $role3 = null, $role4 = null) {
            
            $user = auth()->user();

            if($user){
                
                if($user->role == $role1 || $user->role == $role2 || $user->role == $role3 || $user->role == $role4){
                    
                    return true;
                    
                }

            }

        });


        Blade::if('waiting', function($status){

            if($status == 0){

                return true;

            }

        });

        Blade::if('available', function($available){

            if($available){

                return true;

            }

        });

    }
}
