<?php

namespace App\Http\Controllers;

use App\Container;
use App\Date;
use App\DateContainer;
use App\Http\Requests\ContainerRequest;
use Illuminate\Http\Request;

class ContainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $containers = Container::orderBy('name')->paginate(10);

        return view('app.containers.index', compact('containers'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ContainerRequest $request)
    {

        $date = Date::find($request->date_id);

        $container = Container::create([

            'name'        => $request->name,
            'description' => $request->description,
            'issuing_by'  => $request->issuing_by,
            'padlock'     => $request->padlock,
            'number'      => $request->number,

        ]);

        $date_container = DateContainer::create([

            'date_id'      => $date->id,
            'container_id' => $container->id,

        ]);

        return response()->json($date_container);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function show(Container $container)
    {

        return view('app.containers.show', compact('container'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function edit(Container $container)
    {

        return view('app.containers.edit', compact('container'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function update(ContainerRequest $request, Container $container)
    {

        $container->update($request->all());

        return redirect()
            ->route('containers.show', $container)
            ->with('msg', "La información del contenedor ha sido actualizada.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Container  $container
     * @return \Illuminate\Http\Response
     */
    public function destroy(Container $container)
    {

        $container->delete();

        return redirect()
            ->route('containers.index')
            ->with('warning', "El contenedor ha sido eliminado.");

    }  


}
