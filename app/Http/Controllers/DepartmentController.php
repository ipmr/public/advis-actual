<?php

namespace App\Http\Controllers;

use App\Department;
use App\Http\Requests\DepartmentRequest;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    public function __construct(){

        // Prevenir que solo pueda ingresar un administrador en los siguientes metodos
        $this->middleware('admin')->only('index', 'create', 'store', 'update', 'destroy');

        // Prevenir que un encargado de departamento no pueda ingresar a otros departamentos
        $this->middleware('department-owner')->only('show', 'edit', 'update', 'destroy', 'dates');


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(request()->ajax()){

            $departments = Department::orderBy('name')->get();

            return response()
                ->json($departments);

        }else{

            $departments = Department::orderBy('name')->paginate(10);

            return view('app.departments.index', compact('departments'));

        }

    }

    public function dates(Department $department){

        $dates = $department->dates()->paginate(10);

        return view('app.departments.dates', compact('department', 'dates'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('app.departments.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentRequest $request)
    {
        
        $department = Department::create($request->all());

        return redirect()
            ->route('departments.show', $department)
            ->with('msg', "Se ha creado el departamento {$department->name} exitosamente.");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        
        return view('app.departments.show', compact('department'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        
        return view('app.departments.edit', compact('department'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(DepartmentRequest $request, Department $department)
    {
        
        $department->update($request->all());

        return redirect()
            ->route('departments.show', $department)
            ->with('msg', "La información del departamento ha sido actualizada.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
    
        $name = $department->name;

        $department->hosts()->each(function($host){
            $host->update([
                'department_id' => ''
            ]);
        });

        $department->dates()->each(function($date){
            $date->visitors()->delete();
            $date->delete();
        });

        $department->delete();

        return redirect()
            ->route('departments.index')
            ->with('warning', "El departamento ({$name}) ha sido eliminado.");

    }
}
