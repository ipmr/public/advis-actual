<?php

namespace App\Http\Controllers;

use App\Config;
use App\Date;
use App\DateHistory;
use App\DateVisitor;
use App\Events\HostNeedToKnowWhenVisitorIsArriving;
use App\Http\Requests\DateVisitorRequest;
use App\Notifications\DateFinished;
use App\Notifications\NotifyToHostWhenVisitorIsArriving;
use App\Notifications\SayThankYouToVisitor;
use App\Notifications\WelcomeToVisitor;
use App\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;
use Notification;

class DateVisitorController extends Controller
{

    public function __construct()
    {

        // No se puede modificar un visitante activo
        $this->middleware('date_active')->only([

            'index',
            'store',
            'set_new_visitor_from_date',

        ]);

        // Un guardia en caseta no puede modificar las citas
        $this->middleware('not-guard')->only('index', 'store', 'destroy');

        // No se puede activar una cita que no corresponda al día actual
        $this->middleware('start-date')->only('update');

        // Prevenir el ingreso cuando la cita ya ha expirado
        $this->middleware('date-expired')->only('index', 'store', 'update');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Date $date)
    {

        $date_visitors = $date->visitors;

        return view('app.date-visitors.index', compact('date', 'date_visitors'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Date $date)
    {

        $visitor = $date->company->visitors->find($request->visitor_id);

        if ($visitor) {

            $already_attached = $date->visitors()->where('visitor_id', $visitor->id)->first();

            if ($already_attached) {

                return redirect()
                    ->back()
                    ->with('info', "{$visitor->full_name} ya se encuentra asignado a la cita.");

            } else {

                $date_visitor = DateVisitor::create([

                    'visitor_id' => $visitor->id,
                    'company_id' => $date->company->id,
                    'date_id'    => $date->id,

                ]);

                return redirect()
                    ->back()
                    ->with('msg', "Se ha agreado a {$visitor->full_name} a la cita.");

            }

        } else {

            return redirect()
                ->back()
                ->withErrors([
                    'El visitante seleccionado no es válido',
                ]);

        }

    }

    public function store_pictures(Request $request, DateVisitor $visitor)
    {

        $face_id_img = Image::make($request->face_id);

        $face_id_filename = Str::slug('face id ' . $visitor->date_visitor_id);

        $face_id_img->fit(400, 600);

        $face_id_img->save('avatars-78xtr45/' . $face_id_filename . '.jpg');

        $face_id_base_64 = preg_replace('#^data:image/[^;]+;base64,#', '', base64_encode($face_id_img));

        $card_id = preg_replace('#^data:image/[^;]+;base64,#', '', $request->card_id);

        $visitor->update([

            'face_id' => $face_id_base_64,
            'card_id' => $card_id,

        ]);

        $this->convert_base_64_images_to_jpg_and_saved($request, $visitor);

        return response()->json($visitor);

    }

    public function convert_base_64_images_to_jpg_and_saved($request, $visitor)
    {

        $face_id_img = Image::make($request->face_id);

        $card_id_img = Image::make($request->card_id);

        $face_id_filename = Str::slug('face id ' . $visitor->date_visitor_id);

        $card_id_filename = Str::slug('card id ' . $visitor->date_visitor_id);

        $face_id_img->save('uploads-78xtr45/' . $face_id_filename . '.jpg');

        $card_id_img->save('uploads-78xtr45/' . $card_id_filename . '.jpg');

    }

    public function set_new_visitor_from_date(DateVisitorRequest $request, Date $date)
    {

        $visitor = Visitor::create([

            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'jobtitle'   => $request->jobtitle,
            'company_id' => $date->company->id,
            'gender_id'  => $request->gender_id,

        ]);

        $date_visitor = DateVisitor::create([

            'date_id'    => $date->id,
            'company_id' => $date->company_id,
            'visitor_id' => $visitor->id,

        ]);

        return redirect()
            ->route('dates.visitors.index', $date)
            ->with('msg', "Se ha agreado a {$visitor->full_name} a la cita.");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DateVisitor  $dateVisitor
     * @return \Illuminate\Http\Response
     */
    public function show(Date $date, DateVisitor $visitor)
    {

        $date_visitor = $visitor;

        if (request()->ajax()) {

            if ($date->id == $visitor->date_id) {

                return response()->json($date_visitor);

            }

        } else {

            if ($date->id == $visitor->date_id) {

                return view('app.date-visitors.show', compact('date_visitor'));

            } else {

                return abort(404);

            }

        }

    }

    public function get_barcode_date(Request $request)
    {

        $visitor = DateVisitor::where('date_visitor_id', $request->date_visitor_id)->first();

        if ($visitor) {

            return response()->json($visitor);

        } else {

            $response = [
                'error' => ['No se encontraron resultados'],
            ];

            return response()->json($response, 400);

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DateVisitor  $dateVisitor
     * @return \Illuminate\Http\Response
     */
    public function edit(DateVisitor $visitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DateVisitor  $dateVisitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Date $date, DateVisitor $visitor)
    {

        $visitor->update([

            'status_id' => $request->status_id,

        ]);

        $response = $this->set_response_based_on_visitor_status($date, $visitor);

        $date_status = $this->set_date_status($date);

        return redirect()
            ->back()
            ->with('msg', $response);

    }

    public function update_date_visitor_status_with_barcode(Request $request)
    {

        $date_visitor = DateVisitor::find($request->date_visitor_id);

        $date = $date_visitor->date;

        if ($date_visitor) {

            $date_visitor->update([

                'status_id' => $request->status_id,

            ]);

            $response = $this->set_response_based_on_visitor_status($date, $date_visitor);

            $date_status = $this->set_date_status($date);

            return response()->json($response);

        }

    }

    public function set_date_status($date)
    {

        $visitors = $date->visitors;

        $finished = null;

        foreach ($visitors as $key => $visitor) {

            if ($visitor->status_id == 2) {

                $date->update([

                    'status_id' => 1,

                ]);

            } elseif ($visitor->status_id == 4) {

                $finished = true;

            }

            if ($finished == true && $visitor->status_id != 4) {

                $finished = null;

            }

        }

        if ($finished) {

            $date->update([

                'status_id' => 2,

            ]);

            DateHistory::create([

                'date_id'     => $date->id,
                'description' => 'La cita finalizó.',

            ]);

            $notification_data = [

                'msg' => "Tu cita del día {$date->start_at->format('d/m/Y')} en el horario {$date->start_at->format('h:i a')} ha finalizado."

            ];

            $date->host->user->notify(new DateFinished($notification_data));

        }

    }

    public function set_response_based_on_visitor_status($date, $visitor)
    {

        $response = '';
        $history  = '';

        if ($visitor->status_id == 0) {

            $response = "{$visitor->full_name} esta en espera.";
            $history  = "{$visitor->full_name} tiene una cita en espera.";

        } elseif ($visitor->status_id == 1) {

            $response = "{$visitor->full_name} ha llegado y esta en caseta.";
            $history  = "{$visitor->full_name} llego a las instalaciones.";

            $this->notify_to_host_that_visitor_is_arriving($date, $visitor);

            $this->welcome_to_visitor($date, $visitor);

        } elseif ($visitor->status_id == 2) {

            $response = "{$visitor->full_name} ha ingresado a las instalaciones.";
            $history  = "{$visitor->full_name} ingresó a las instalaciones.";

        } elseif ($visitor->status_id == 3) {

            $response = "{$visitor->full_name} ha pausado su cita.";
            $history  = "{$visitor->full_name} realizó una pausa en su cita.";

        } elseif ($visitor->status_id == 4) {

            $response = "{$visitor->full_name} ha finalizado su cita.";
            $history  = "{$visitor->full_name} finalizó su cita";

            $this->say_thank_you_to_visitor($date, $visitor);

        }

        DateHistory::create([

            'date_id'     => $date->id,
            'description' => $history,

        ]);

        return $response;

    }

    public function say_thank_you_to_visitor($date, $visitor){

        if($visitor->visitor->email){

            $notification_data = [
                'msg' => "{$visitor->full_name}, tu cita ha finalizado, te deseamos un excelente día." 
            ];

            Notification::route('mail', $visitor->visitor->email)
                ->notify(new SayThankYouToVisitor($notification_data));

        }

    }


    public function welcome_to_visitor($date, $visitor)
    {

        if ($visitor->visitor->email) {

            $company = Config::first();

            $welcome_by_gender = $visitor->visitor->gender_id == 0 ? 'Bienvenido' : 'Bienvenida';

            $notification_data = [

                'welcome_by_gender' => $welcome_by_gender,
                'msg'               => "{$visitor->full_name}, {$welcome_by_gender} a {$company->name}, en un momento se te atenderá.",

            ];

            Notification::route('mail', $visitor->visitor->email)
                ->notify(new WelcomeToVisitor($notification_data));

        }

    }

    public function notify_to_host_that_visitor_is_arriving($date, $visitor)
    {

        $host = $date->host->user;

        $notification_data = [

            'msg' => "{$visitor->full_name} esta en caseta esperando para iniciar su cita.",

        ];

        $host->notify(new NotifyToHostWhenVisitorIsArriving($notification_data));

        event(new HostNeedToKnowWhenVisitorIsArriving($notification_data));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DateVisitor  $dateVisitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Date $date, DateVisitor $visitor)
    {

        if ($date->visitors->count() == 1) {

            return redirect()
                ->back()
                ->withErrors([
                    'Debe haber al menos un vitante para esta cita.',
                ]);

        } else {

            $name = $visitor->full_name;

            $visitor->delete();

            return redirect()
                ->back()
                ->with('warning', "Se ha removido a {$name} de la cita.");

        }

    }
}
