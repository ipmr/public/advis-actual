<?php

namespace App\Http\Controllers;

use App\Company;
use App\Date;
use App\DateVehicle;
use App\DateVehicleComment;
use App\DateVehicleHistory;
use App\DateVehicleImage;
use App\Http\Requests\SearchVehicleRequest;
use App\Http\Requests\VehicleRequest;
use App\Vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {

        $vehicles = $company
            ->vehicles()
            ->orderBy('brand')
            ->orderBy('model')
            ->paginate(10);

        return view('app.vehicles.index', compact('company', 'vehicles'));

    }

    public function all()
    {

        $vehicles = Vehicle::orderBy('brand')->orderBy('model')->paginate(10);

        return view('app.vehicles.all', compact('vehicles'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {

        return view('app.vehicles.create', compact('company'));

    }

    public function new_vehicle()
    {

        $companies = Company::where('id', '<>', 1)
            ->orderBy('name')
            ->get();

        return view('app.vehicles.new', compact('companies'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehicleRequest $request, Company $company)
    {

        $vehicle = Vehicle::create([

            'company_id' => $company->id,
            'type_id'    => $request->type_id,
            'brand'      => $request->brand,
            'model'      => $request->model,
            'year'       => $request->year,
            'plates'     => $request->plates,
            'color'      => $request->color,

        ]);

        return redirect()
            ->route('companies.vehicles.show', [$company, $vehicle])
            ->with('msg', "Se ha agregado un nuevo vehículo exitosamente.");

    }

    public function store_vehicle(VehicleRequest $request)
    {

        $company_id = $this->validate_if_vehicle_if_from_company_or_independent($request);

        $vehicle = $this->create_new_vehicle($request, $company_id);

        return redirect()
            ->route('companies.vehicles.show', [$vehicle->company, $vehicle])
            ->with('msg', 'Se ha agreado un nuevo vehículo exitosamente.');

    }

    public function create_new_vehicle($request, $company_id)
    {

        $vehicle = Vehicle::create([

            'company_id' => $company_id,
            'type_id'    => $request->type_id,
            'brand'      => $request->brand,
            'model'      => $request->model,
            'year'       => $request->year,
            'plates'     => $request->plates,
            'color'      => $request->color,

        ]);

        return $vehicle;

    }

    public function validate_if_vehicle_if_from_company_or_independent($request)
    {

        $company_id = '';

        if ($request->has('independent_company') || $request->company_id == '') {
            $company_id = 1;
        } else {
            $company_id = $request->company_id;
        }

        return $company_id;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company, Vehicle $vehicle)
    {

        $vehicle_dates = $vehicle->dates()->pluck('date_id')->toArray();

        $dates = Date::whereIn('id', $vehicle_dates)->paginate(10);

        return view('app.vehicles.show', compact('company', 'vehicle', 'dates'));

    }

    public function check_vehicle()
    {

        return view('app.vehicles.check');

    }

    public function search_vehicle_plates(SearchVehicleRequest $request)
    {

        $vehicle = Vehicle::where('plates', $request->plates)->first();

        $date = $this->search_for_next_vehicle_date($vehicle);

        if ($date) {

            $date_vehicle = DateVehicle::where('date_id', $date->id)->where('vehicle_id', $vehicle->id)->first();

            return view('app.vehicles.search-result', compact('vehicle', 'date', 'date_vehicle'));

        } else {

            return redirect()
                ->back()
                ->with('warning', 'El vehículo no tiene citas agendadas');

        }

    }

    public function search_for_next_vehicle_date($vehicle)
    {

        $today = Carbon::today()->format('Y-m-d');

        $dates_ids = $vehicle->dates()->pluck('date_id')->toArray();

        $next_date = Date::whereIn('id', $dates_ids)
            ->where('start_at', '>=', $today)
            ->where('status_id', 0)
            ->first();

        return $next_date;

    }

    public function update_date_vehicle(Request $request, DateVehicle $date_vehicle)
    {

        $this->update_date_vehicle_status($request, $date_vehicle);

        $this->save_date_vehicle_images($request, $date_vehicle);

        $this->set_date_vehicle_comment($request, $date_vehicle);

        return redirect()
            ->back()
            ->with('msg', "La información ha sido actualizada");

    }

    public function set_date_vehicle_comment($request, $date_vehicle)
    {

        $user = auth()->user();

        if ($request->comment) {

            $comment = DateVehicleComment::create([

                'user_id'         => $user->id,
                'date_vehicle_id' => $date_vehicle->id,
                'comment'         => $request->comment,

            ]);

        }

    }

    public function save_date_vehicle_images($request, $date_vehicle)
    {

        if ($request->has('images')) {

            foreach ($request->file('images') as $key => $img) {

                $image = Image::make($img);

                $image->resize(null, 900, function ($constraint) {
                    $constraint->aspectRatio();
                });

                $file_name = Str::slug($date_vehicle->id . '-' . hexdec(uniqid())) . '.jpg';

                $image->save('vehicles-78xtr45/' . $file_name);

                DateVehicleImage::create([

                    'date_vehicle_id' => $date_vehicle->id,
                    'pathname'        => $file_name,

                ]);

            }

            $img_total = count($request->file('images'));

            $history_description = '';

            if($img_total == 1){

                $history_description = 'Se agrego 1 nueva imagen del vehículo';

            }else{

                $history_description = "Se agregaron {$img_total} nuevas del vehículo";

            }

            DateVehicleHistory::create([
                'description'     => $history_description,
                'date_vehicle_id' => $date_vehicle->id,
            ]);

        }

    }

    public function update_date_vehicle_status($request, $date_vehicle)
    {

        if ($request->has('status_id')) {

            $current_status = $date_vehicle->status_id;

            if($current_status != $request->status_id){

                $date_vehicle->update([
                    'status_id' => $request->status_id,
                ]);

                $history_description = '';

                if ($request->status_id == 0) {

                    $history_description = 'El vehículo esta en espera';

                } elseif ($request->status_id == 1) {

                    $history_description = 'El vehículo ingresó a las instalaciones';

                } elseif ($request->status_id == 2) {

                    $history_description = 'El vehículo salió de las instalaciones';

                }

                DateVehicleHistory::create([

                    'description'     => $history_description,
                    'date_vehicle_id' => $date_vehicle->id,

                ]);

            }


        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company, Vehicle $vehicle)
    {

        $companies = Company::where('id', '<>', 1)
            ->orderBy('name')
            ->get();

        return view('app.vehicles.edit', compact('companies', 'company', 'vehicle'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(VehicleRequest $request, Company $company, Vehicle $vehicle)
    {

        $vehicle->update([

            'type_id' => $request->type_id,
            'brand'   => $request->brand,
            'model'   => $request->model,
            'year'    => $request->year,
            'plates'  => $request->plates,
            'color'   => $request->color,

        ]);

        return redirect()
            ->route('companies.vehicles.show', [$company, $vehicle])
            ->with('msg', "La información del vehículo ha sido actualizada.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company, Vehicle $vehicle)
    {

        $vehicle->dates()->delete();

        $vehicle->delete();

        return redirect()
            ->route('companies.vehicles.index', $company)
            ->with('msg', 'El vehículo ha sido eliminado exitosamente.');

    }

    public function remove_date_vehicle_image(DateVehicleImage $image)
    {

        $image->delete();

        return redirect()
            ->back()
            ->with('msg', 'La imagen del vehículo ha sido eliminada');

    }

}
