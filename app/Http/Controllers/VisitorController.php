<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\VisitorRequest;
use App\Visitor;
use Illuminate\Http\Request;

class VisitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Company $company)
    {

        if (request()->ajax()) {

            $visitors = $company->visitors;

            return response()->json($visitors);

        } else {

            $visitors = $company->visitors()->paginate(10);

            return view('app.visitors.index', compact('company', 'visitors'));

        }

    }


    public function all(){

        $visitors = Visitor::orderBy('first_name')->paginate(10);

        return view('app.visitors.all', compact('visitors'));

    }


    public function new_visitor(){

        $companies = Company::where('id', '<>', 1)
            ->orderBy('name')
            ->get();

        return view('app.visitors.new', compact('companies'));

    }

    public function store_visitor(VisitorRequest $request){

        
        $visitor = Visitor::create($request->except('independent_visitor'));


        if($request->independent_visitor || $request->company_id == 1){

            $visitor->update([

                'company_id' => 1

            ]);

        }else{

            $visitor->update([

                'company_id' => $request->company_id

            ]);

        }


        return redirect()->route('companies.visitors.show', [$visitor->company, $visitor]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Company $company)
    {
        
        return view('app.visitors.create', compact('company'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitorRequest $request, Company $company)
    {

        $visitor = Visitor::create([

            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'jobtitle'   => $request->jobtitle,
            'type_id'    => $request->type_id,
            'gender_id'  => $request->gender_id,
            'company_id' => $company->id,

        ]);

        if(request()->ajax()){

            return response()->json($visitor);

        }else{

            return redirect()
                ->route('companies.visitors.index', $company)
                ->with('msg', "Se ha creado el visitante {$visitor->full_name} de la compañía {$company->name}");

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company, Visitor $visitor)
    {
        if(request()->ajax()){

            return response()
                ->json($visitor);

        }else{

            return view('app.visitors.show', compact('company', 'visitor'));

        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company, Visitor $visitor)
    {

        return view('app.visitors.edit', compact('company', 'visitor'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(VisitorRequest $request, Company $company, Visitor $visitor)
    {
        
        $visitor->update($request->all());

        return redirect()
            ->route('companies.visitors.show', [$company, $visitor])
            ->with('msg', "Se ha actualizado la información del visitante.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company, Visitor $visitor)
    {   
        $name = $visitor->full_name;

        $visitor->dates()->delete();

        $visitor->delete();

        return redirect()->route('companies.visitors.index', $company)
            ->with('msg', "Se ha removido al visitante {$name} exitosamente");

    }
}
