<?php

namespace App\Http\Controllers;

use App\Date;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $user = auth()->user();


        if($user->role == 'admin' || $user->role == 'guard'){

            
            $dates = app('App\Http\Controllers\DateController')->daily();

            $indicators = app('App\Http\Controllers\DateController')->indicators();

            return view('app.dashboard.admin', compact('dates', 'indicators'));


        }elseif($user->role == 'host'){

            $host = $user->host;

            $today = Carbon::today()->format('Y-m-d');

            $dates = $host->dates()->whereDate('start_at', $today)->paginate(10);

            return view('app.dashboard.host', compact('host', 'dates'));

        }elseif($user->role == 'terminal'){


        }

    }

}
