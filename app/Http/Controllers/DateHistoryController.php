<?php

namespace App\Http\Controllers;

use App\DateHistory;
use Illuminate\Http\Request;

class DateHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DateHistory  $dateHistory
     * @return \Illuminate\Http\Response
     */
    public function show(DateHistory $dateHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DateHistory  $dateHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(DateHistory $dateHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DateHistory  $dateHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DateHistory $dateHistory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DateHistory  $dateHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(DateHistory $dateHistory)
    {
        //
    }
}
