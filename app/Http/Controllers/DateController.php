<?php

namespace App\Http\Controllers;

use App\Company;
use App\Date;
use App\DateHistory;
use App\DateVehicle;
use App\DateVisitor;
use App\Http\Requests\DateRequest;
use App\Http\Requests\UpdateDateRequest;
use App\Http\Requests\VehicleRequest;
use App\Notifications\NewDate;
use App\Vehicle;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Notification;

class DateController extends Controller
{

    public function __construct()
    {

        // No podemos editar la cita si esta activada
        $this->middleware('date_active')->only('edit', 'update', 'destroy');

        // Se requiere al menos un departamento para poder crear una cita
        $this->middleware('department')->only('create');

        // Se requiere al menos un host para poder crear una cita
        $this->middleware('host')->only('create');

        // Los guardias en caseta no pueden editar las citas
        $this->middleware('not-guard')->only('edit', 'update', 'destroy');

        // Prevenir la edicion de una cita cuando haya expirado
        $this->middleware('date-expired')->only('edit', 'add_existing_vehicle', 'add_new_vehicle');

        // Prevenir que un host ingrese a una cita que no le pertenece
        $this->middleware('date-owner')->only('show', 'update', 'destroy');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  

        $user = auth()->user();

        $role = $user->role;

        $dates = null;

        if($role == 'admin' || $role == 'guard'){

            $dates = Date::orderBy('start_at')->paginate(10);

        }elseif($role == 'host'){

            $dates = Date::orderBy('start_at')->where('host_id', $user->host->id)->paginate(10);

        }


        return view('app.dates.index', compact('dates'));

    }

    public function type(){

        return view('app.dates.type');

    }

    public function daily()
    {

        $today = Carbon::today()->format('Y-m-d');

        $dates = Date::whereDate('start_at', $today)
            ->whereIn('status_id', [0, 1])
            ->orderBy('start_at')
            ->paginate(10);

        return $dates;

    }

    public function search(Request $request)
    {   

        $user = auth()->user();

        $role = $user->role;

        $query = request('query');

        $visitors = Visitor::where('first_name', 'like', "%{$query}%")
            ->orWhere('last_name', 'like', "%{$query}%")
            ->pluck('id')->toArray();

        $companies = Company::where('name', 'like', "%{$query}%")
            ->pluck('id')->toArray();

        $date_visitors = DateVisitor::whereIn('visitor_id', $visitors)
            ->orWhereIn('company_id', $companies)
            ->pluck('date_id');

        
        if($role == 'admin' || $role == 'guard'){

            $dates = Date::whereIn('id', $date_visitors)->paginate(10);

        }elseif($role == 'host'){

            $dates = Date::whereIn('id', $date_visitors)->where('host_id', $user->host->id)->paginate(10);

        }

        return view('app.dates.search', compact('dates', 'query'));

    }

    public function indicators()
    {

        $today = Carbon::today()->format('Y-m-d');

        $response = [

            [
                'name'  => 'Citas para hoy',
                'count' => Date::whereDate('start_at', $today)->count(),
            ],
            [
                'name'  => 'Citas en espera',
                'count' => Date::whereDate('start_at', $today)->where('status_id', 0)->count(),
            ],
            [
                'name'  => 'Citas en curso',
                'count' => Date::whereDate('start_at', $today)->where('status_id', 1)->count(),
            ],
            [
                'name'  => 'Citas finalizadas',
                'count' => Date::whereDate('start_at', $today)->where('status_id', 2)->count(),
            ],

        ];

        return collect($response);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('app.dates.create');

    }

    public function container(){
        return view('app.dates.container');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DateRequest $request)
    {

        $start_at = $this->format_start_at_date($request);

        $valid = $this->validate_if_date_has_visitors($request);

        if ($valid) {

            $date = $this->build_new_date($request, $start_at);

            $visitors = $this->add_visitors_to_new_date($date, $request);

            return response()->json($date);

        } else {

            $response = [

                'errors' => [
                    'visitantes' => ['Por favor agrega al menos un visitante'],
                ],

            ];

            return response()->json($response, 400);

        }

    }

    public function add_visitors_to_new_date($date, $request)
    {

        $visitors = json_decode($request->visitors, true);

        foreach ($visitors as $key => $visitor) {

            $visitor = Visitor::find($visitor['id']);

            $date_visitor = DateVisitor::create([

                'date_id'         => $date->id,
                'company_id'      => $date->company->id,
                'visitor_id'      => $visitor->id,
                'date_visitor_id' => hexdec(uniqid()),

            ]);


            if($visitor->email){

                $notification_data = [

                    'msg' => "{$visitor->full_name}, se te ha agendado una cita para el día {$date->start_at->format('d/m/Y')} en el horario {$date->start_at->format('h:i a')}, el motivio de la cita es: {$date->subject}"

                ];

                Notification::route('mail', $visitor->email)
                    ->notify(new NewDate($notification_data));

            }

        }

    }

    public function build_new_date($request, $start_at)
    {

        $date = Date::create([

            'subject'       => $request->subject,
            'description'   => $request->description,
            'company_id'    => $request->company_id,
            'department_id' => $request->department_id,
            'host_id'       => $request->host_id,
            'start_at'      => $start_at,
            'vigency'       => $request->vigency,

        ]);

        DateHistory::create([

            'date_id'     => $date->id,
            'description' => 'Se creó la cita',

        ]);

        return $date;

    }

    public function validate_if_date_has_visitors($request)
    {

        $visitors = json_decode($request->visitors, true);

        return count($visitors) == 0 ? null : true;

    }

    public function format_start_at_date($request)
    {

        $date = str_replace('/', '-', $request->date);

        $date = Carbon::parse($date);

        return $date;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function show(Date $date)
    {

        return view('app.dates.show', compact('date'));

    }


    public function get_date(Date $date){

        $response = [

            'date'             => $date,
            'vehicles'         => $date->vehicles,
            'company_vehicles' => $date->company->vehicles,
            'visitors'         => $date->visitors,

        ];

        return response()->json($date);

    }

    public function add_existing_vehicle(Request $request, Date $date)
    {

        $vehicle_if_from_date_company = $this->validate_if_vehicle_is_from_date_company($request, $date);

        if ($vehicle_if_from_date_company) {

            $is_already_assigned = $this->validate_if_vehicle_is_already_assigned_to_date($request, $date);

            if($is_already_assigned){

                $response = [

                    'errors' => ['El vehículo ya se encuentra agregado a la cita.']

                ];

                return response()->json($response, 400);

            }else{

                $vehicle = $this->attach_vehicle_to_date($request->vehicle_id, $date);

                return response()->json($vehicle);

            }


        }

    }

    public function validate_if_vehicle_is_already_assigned_to_date($request, $date){

        $find_vehicle_in_date = $date->vehicles()->where('vehicle_id', $request->vehicle_id)->first();

        return $find_vehicle_in_date ? true : false;

    }

    public function attach_vehicle_to_date($vehicle_id, $date)
    {

        $date_vehicle = DateVehicle::create([

            'vehicle_id' => $vehicle_id,
            'date_id'    => $date->id,

        ]);

        return $date_vehicle;

    }

    public function validate_if_vehicle_is_from_date_company($request, $date)
    {

        $company = $date->company;

        $vehicle = $company->vehicles()->find($request->vehicle_id);

        return $vehicle ? true : false;

    }

    public function add_new_vehicle(VehicleRequest $request, Date $date)
    {

        $vehicle = Vehicle::create([

            'company_id' => $date->company_id,
            'type_id'    => $request->type_id,
            'brand'      => $request->brand,
            'model'      => $request->model,
            'year'       => $request->year,
            'plates'     => $request->plates,
            'color'      => $request->color,

        ]);

        $vehicle = $this->attach_vehicle_to_date($vehicle->id, $date);

        return response()->json($vehicle);

    }

    public function remove_vehicle(DateVehicle $date_vehicle, Date $date){


        $vehicle_if_from_date = $this->validate_if_vehicle_if_attached_to_date($date_vehicle, $date);

        if($vehicle_if_from_date){

            $date_vehicle->images()->delete();

            $date_vehicle->comments()->delete();

            $date_vehicle->delete();


            if(request()->ajax()){

                return response()->json($date);

            }else{

                return redirect()
                    ->route('dates.show', $date)
                    ->with('msg', 'El vehículo ha sido removido de la cita');

            }


        }


    }


    public function validate_if_vehicle_if_attached_to_date($date_vehicle, $date){

        $attached = $date->vehicles()->find($date_vehicle->id);

        return $attached ? true : false;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function edit(Date $date)
    {

        return view('app.dates.edit', compact('date'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDateRequest $request, Date $date)
    {

        $start_at = Carbon::parse($request->start_at);

        $date->update([

            'subject'     => $request->subject,
            'description' => $request->description,
            'start_at'    => $start_at,
            'vigency'     => $request->vigency,

        ]);

        return redirect()
            ->route('dates.show', $date)
            ->with('msg', 'La información de la cita ha sido modificada.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Date  $date
     * @return \Illuminate\Http\Response
     */
    public function destroy(Date $date)
    {

        $date->visitors()->delete();

        $date->delete();

        return redirect()->route('dates.index')->with('msg', 'La cita ha sido eliminada.');

    }
}
