<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(){

        $this->middleware('notifications-owner')
            ->only('unread_notifications', 'notifications');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::where('role', '<>', 'host')
            ->orderBy('role')
            ->orderBy('first_name')
            ->paginate();

        return view('app.users.index', compact('users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('app.users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $user = User::create([

            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'role'       => $request->role,
            'password'   => \Hash::make($request->password),

        ]);

        return redirect()
            ->route('users.show', $user)
            ->with('msg', "Se ha creado el usuario ({$user->full_name}) exitosamente");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        if ($user->role == 'host') {

            return abort(404);

        }

        return view('app.users.show', compact('user'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        if ($user->role == 'host') {

            return abort(404);

        }

        return view('app.users.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {

        $can_i_change_role = $this->validate_if_there_is_more_than_one_admin($request, $user);

        if ($can_i_change_role) {

            $user->update([

                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'email'      => $request->email,
                'role'       => $request->role,

            ]);

            if ($request->password) {

                $user->update([

                    'password' => \Hash::make($request->password),

                ]);

            }

            return redirect()
                ->route('users.show', $user)
                ->with('msg', "La información del usuario ha sido actualizada");

        } else {

            return redirect()
                ->back()
                ->withErrors(['No puedes modificar el role del usuario porque es el único administrador en el sistema.']);

        }

    }

    public function validate_if_there_is_more_than_one_admin($request, $user)
    {

        if ($user->role == 'admin' && $request->role == 'terminal') {

            $admin_count = User::where('role', 'admin')->count();

            return $admin_count > 1 ? true : false;

        } else {

            return true;

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $name = $user->full_name;

        $can_i_delete_user = $this->validate_if_i_can_delete_user($user);

        if ($can_i_delete_user) {

            if ($user->host) {

                $user->host->delete();

            }

            $user->delete();

            return redirect()
                ->route('users.index')
                ->with('warning', "Se ha eliminado a {$name} del sistema.");

        } else {

            return redirect()
                ->back()
                ->withErrors(['No puedes eliminar a este usuario ya que es el único administrador en el sistema.']);

        }

    }

    public function validate_if_i_can_delete_user($user)
    {

        if ($user->role == 'admin') {

            $admin_count = User::where('role', 'admin')->count();

            return $admin_count > 1 ? true : false;

        } else {

            return true;

        }

    }

    public function unread_notifications(User $user){

        $notifications = $user->unreadNotifications;
        
        return response()->json($notifications);   

    }

    public function notifications(User $user){

        $notifications = $user->notifications;

        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        return view('app.users.notifications', compact('notifications'));

    }

}
