<?php

namespace App\Http\Controllers;

use App\Config;
use App\Http\Requests\ConfigRequest;

class ConfigController extends Controller
{

    public function __construct(){


        $this->middleware('admin');


    }

    public function index()
    {

        $config = Config::first();

        return view('app.configuration.index', compact('config'));

    }

    public function update(ConfigRequest $request)
    {

        $config = Config::first();

        $config->update([

            'name'    => $request->name,
            'address' => $request->address,
            'phone'   => $request->phone,
            'rfc'     => $request->rfc,

        ]);

        return redirect()
        	->back()
        	->with('msg', "La información ha sido actualizada.");

    }

}
