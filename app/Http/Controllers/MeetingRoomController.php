<?php

namespace App\Http\Controllers;

use App\Http\Requests\MeetingRoomRequest;
use App\MeetingRoom;
use Illuminate\Http\Request;

class MeetingRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $meeting_rooms = MeetingRoom::orderBy('name')->paginate(10);

        return view('app.meeting-rooms.index', compact('meeting_rooms'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('app.meeting-rooms.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MeetingRoomRequest $request)
    {
        
        $meeting_room = MeetingRoom::create($request->all());

        return redirect()
            ->route('meeting-rooms.show', $meeting_room)
            ->with('msg', "Se ha creado la sala de juntas ({$meeting_room->name})");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MeetingRoom  $meetingRoom
     * @return \Illuminate\Http\Response
     */
    public function show(MeetingRoom $meeting_room)
    {
        
        return view('app.meeting-rooms.show', compact('meeting_room'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MeetingRoom  $meetingRoom
     * @return \Illuminate\Http\Response
     */
    public function edit(MeetingRoom $meeting_room)
    {
        
        return view('app.meeting-rooms.edit', compact('meeting_room'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MeetingRoom  $meetingRoom
     * @return \Illuminate\Http\Response
     */
    public function update(MeetingRoomRequest $request, MeetingRoom $meeting_room)
    {
        
        $meeting_room->update($request->all());

        return redirect()
            ->route('meeting-rooms.show', $meeting_room)
            ->with('msg', "La información de la sala de juntas ha sido actualizada.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MeetingRoom  $meetingRoom
     * @return \Illuminate\Http\Response
     */
    public function destroy(MeetingRoom $meeting_room)
    {
        
        $name = $meeting_room->name;

        $meeting_room->delete();

        return redirect()
            ->route('meeting-rooms.index')
            ->with('warning', "La sala de juntas ({$name}) ha sido eliminada.");

    }
}
