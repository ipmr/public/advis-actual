<?php

namespace App\Http\Middleware;

use Closure;

class DateActiveMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $date = request('date');

        // dd($request->all());

        if($date->status_id > 0){

            return redirect()->back()->withErrors(['La cita se encuentra en curso o ha sido finalizada, por lo tanto no puede ser editada']);

        }else{

            return $next($request);

        }

    }
}
