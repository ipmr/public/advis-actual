<?php

namespace App\Http\Middleware;

use Closure;

class DateExpiredMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $date = request('date');

        if($date->is_available){

            return $next($request);

        }else{

            return redirect()
                ->back()
                ->withErrors([
                    'Esta cita ha expirado, por lo tanto no puede ser modificada.'
                ]);

        }

    }
}
