<?php

namespace App\Http\Middleware;

use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();

        $role = $user->role;

        if($role == 'admin'){

            return $next($request);

        }else{

            return redirect()
                ->back()
                ->with('warning', 'No tienes permisos para ingresar a esta área');

        }

    }
}
