<?php

namespace App\Http\Middleware;

use App\Host;
use Closure;

class HostMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $find_host = Host::first();

        if($find_host){

            return $next($request);

        }else{

            return redirect()
                ->route('departments.index')
                ->with('warning', 'Por favor agrega a un encargado en un departamento para continuar.');

        }

    }
}
