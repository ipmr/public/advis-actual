<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject'       => 'required|string',
            'description'   => 'string|nullable',
            'date'          => 'required',
            'vigency'       => 'required|in:1,2,5,8,0',
            'company_id'    => 'required|exists:companies,id',
            'department_id' => 'required|exists:departments,id',
            'host_id'       => 'required|exists:hosts,id'
        ];
    }

    public function messages()
    {

        return [

            'subject.required'       => 'El asunto de la cita es requerido',
            'subject.string'         => 'El asunto de la cita no es válido',
            'description.string'     => 'La descripción de la cita no es valida',
            'date.required'          => 'La fecha de la cita es requerida',
            'vigency.required'       => 'La vigencia es requerida',
            'vigency.in'             => 'La vigencia no es válida',
            'company_id.required'    => 'Por favor define si la cita es de una compañía ó independiente',
            'company_id.exists'      => 'La compañía proporcionada no es válida',
            'department_id.required' => 'El departamento es requerido',
            'department_id.exists'   => 'El departamento seleccionado no existe',
            'host_id.required'       => 'El visitado es requerido',
            'host_id.exists'         => 'El visitado seleccionado no existe'

        ];

    }
}
