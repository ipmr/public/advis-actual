<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DepartmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $department = request('department');

        return [

            'name' => ['required', Rule::unique('departments')->ignore($department ? $department->id : '')],

        ];
    }

    public function messages()
    {

        return [

            'name.required' => 'El nombre del departamento es requerido',
            'name.unique'   => 'El nombre del departamento no esta disponible',

        ];

    }

}
