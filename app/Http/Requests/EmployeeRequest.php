<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $employee = request('employee');

        $request = request();

        return [
            'first_name'    => 'required|string',
            'last_name'     => 'required|string',
            'email'         => [
                'required',
                'email',
                Rule::unique('employees')->ignore($employee ? $employee->id : ''),
                Rule::unique('users')->ignore($employee ? optional(optional($employee->host)->user)->id : ''),
            ],
            'jobtitle'      => 'required|string',
            'phone'         => 'string|nullable',
            'ext'           => 'string|nullable',
            'mobile'        => 'string|nullable',
            'gender_id'     => 'required|in:0,1',
            'employee_id'   => [Rule::requiredIf(function () use ($employee) {

                return $employee ? true : false;

            })],
            'status_id'     => 'required|in:0,1,2,3,4,5,6',
            'department_id' => ['exists:departments,id', Rule::requiredIf(function () use ($request) {

                return $request->has('department_id');

            })],
        ];
    }

    public function messages()
    {

        return [

            'first_name.required'    => 'El nombre del empleado es requerido',
            'last_name.required'     => 'El apellido del empleado es requerido',
            'email.required'         => 'El correo electrónico es requerido',
            'email.email'            => 'El correo electrónico no es válido',
            'email.unique'           => 'El correo electrónico no esta disponible',
            'jobtitle.required'      => 'El puesto laboral es requerido',
            'phone.string'           => 'El telefono no es válido',
            'ext.string'             => 'La extensión no es válida',
            'mobile.string'          => 'El celular no es válido',
            'gender_id.required'     => 'El género es requerido',
            'gender_id.in'           => 'El género proporcionado no es válido',
            'employee_id.required'   => 'El ID del empleado es requerido',
            'status_id.required'     => 'El status del empleado es requerido',
            'status_id.in'           => 'El status proporcionado no es válido',
            'department_id.required' => 'El departamento es requerido',
            'department.in'          => 'El departamento proporcionado no es válido',

        ];

    }
}
