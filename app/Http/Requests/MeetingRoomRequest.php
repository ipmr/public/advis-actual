<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MeetingRoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $meeting_room = request('meeting_room');

        return [

            'name'        => ['required', Rule::unique('meeting_rooms')->ignore($meeting_room ? $meeting_room->id : '')],
            'description' => 'string|nullable',
            'location'    => 'string|nullable',

        ];
    }

    public function messages()
    {

        return [

            'name.required'      => 'El nombre es requerido',
            'name.unique'        => 'El nombre no esta disponible',
            'description.string' => 'La descripción no es válida',
            'location.string'    => 'La ubicación no es válida',

        ];

    }

}
