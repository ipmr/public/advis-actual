<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DateVisitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => 'email|nullable',
            'jobtitle'   => 'string|nullable',
            'gender_id'  => 'required|in:0,1',

        ];
    }

    public function messages()
    {

        return [

            'first_name.required' => 'El nombre del visitante es requerido',
            'first_name.string'   => 'El nombre del visitante no es válido',
            'last_name.required'  => 'El apellido del visitante es requerido',
            'last_name.string'    => 'El apellido del visitante no es válido',
            'email.email'         => 'El correo electrónico proporcionado no es válido',
            'jobtitle.string'     => 'El puesto laboral proporcionado no es válido',
            'gender_id.required'  => 'El género es requerido',
            'gender_id.in'        => 'El género proporcionado no es válido',

        ];

    }

}
