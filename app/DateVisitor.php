<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateVisitor extends Model
{
    
	protected $guarded = [];

	protected $hidden = [
		'company',
		'visitor',
		'date'
	];

	protected $appends = [

		'avatar',
		'full_name',
		'company_name',
		'start_at',
		'end_at',
		'department_name',
		'status_badge',
		'status'

	];

	public function visitor(){

		return $this->belongsTo(Visitor::class, 'visitor_id');

	}

	public function company(){

		return $this->belongsTo(Company::class, 'company_id');

	}

	public function getCompanyNameAttribute(){

		return $this->company->name;

	}

	public function getFullNameAttribute(){

		return $this->visitor->full_name;

	}

	public function getEmailAttribute(){

		return $this->visitor->email ? $this->visitor->email : 'Sin correo electrónico';

	}

	public function getJobtitleAttribute(){

		return $this->visitor->jobtitle ? $this->visitor->jobtitle : 'Sin puesto laboral';

	}

	public function getStatusAttribute(){

		switch ($this->status_id) {
			case 0:
				return 'En espera';
				break;
			
			case 1:
				return 'En caseta';
				break;

			case 2:
				return 'En curso';
				break;

			case 3:
				return 'En pausa';
				break;

			case 4:
				return 'Cita finalizada';
				break;
		}

	}


	public function getStatusBadgeAttribute(){

		switch ($this->status_id) {
			case 0:
				return '<span class="badge badge-pill w-100 badge-secondary">En espera</span>';
				break;
			
			case 1:
				return '<span class="badge badge-pill w-100 badge-info">En caseta</span>';
				break;

			case 2:
				return '<span class="badge badge-pill w-100 badge-success">En curso</span>';
				break;

			case 3:
				return '<span class="badge badge-pill w-100 badge-warning">En pausa</span>';
				break;

			case 4:
				return '<span class="badge badge-pill w-100 badge-primary">Finalizada</span>';
				break;
		}

	}


	public function date(){

		return $this->belongsTo(Date::class, 'date_id');

	}

	public function getStartAtAttribute(){

		return $this->date->start;

	}

	public function getEndAtAttribute(){

		return $this->date->end;

	}

	public function getDepartmentNameAttribute(){

		return $this->date->department_name;

	}

	public function getCardAttribute(){

		if($this->card_id){

			return asset('uploads-78xtr45/card-id-' . $this->date_visitor_id . '.jpg');	

		}else{

			return asset('img/undraw_profile_6l1l.svg');

		}


	}

	public function getAvatarAttribute(){

		$gender = $this->visitor->gender_id;

		if($this->face_id){

			return asset('uploads-78xtr45/face-id-' . $this->date_visitor_id . '.jpg');

		}elseif($gender == 0){

			return asset('img/undraw_male_avatar_323b.svg');

		}elseif($gender == 1){

			return asset('img/undraw_female_avatar_l3ey.svg');

		}

	}

}
