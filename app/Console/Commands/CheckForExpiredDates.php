<?php

namespace App\Console\Commands;

use App\Date;
use App\Events\DatesWillExpire;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckForExpiredDates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dates:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Busca las citas que estan por finalizar para notificar al agente en caseta';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $dates = $this->search_for_dates_in_progress_with_10_minutes_left();

        $send_notifications = $this->notify_about_dates_to_expire($dates);

    }

    public function search_for_dates_in_progress_with_10_minutes_left()
    {

        $current_time = Carbon::now();

        $dates = collect([]);

        $dates_in_progress = Date::where('status_id', 1)
            ->whereDate('start_at', $current_time->format('Y-m-d'))
            ->get();

        if (count($dates_in_progress)) {

            foreach ($dates_in_progress as $key => $date) {

                $date_end_at = $date->end_without_format;

                $diff_in_minutes = $date_end_at->diffInMinutes($current_time);

                if ($diff_in_minutes >= 10) {

                    $dates->push($date);

                }

            }

        } else {

            print('No hay citas en curso');

        }

        return $dates;

    }

    public function notify_about_dates_to_expire($dates)
    {

        if (count($dates)) {

            event(new DatesWillExpire($dates));

        } else {

            print('Las citas en curso aun tienen mas de 10 minutos disponibles.');

        }

    }

}
