<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRoom extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [];

	public function dates(){

		return $this->hasMany(DateMeetingRoom::class, 'meeting_room_id');

	}

	public function getDatesTotalAttribute(){

		return '('.$this->dates()->count().') Citas';

	}	

}
