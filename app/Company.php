<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [];

	public function dates(){

		return $this->hasMany(Date::class, 'company_id');

	}

	public function getDatesTotalAttribute(){

		return '('. $this->dates()->count() .') Citas';

	}


	public function visitors(){

		return $this->hasMany(Visitor::class, 'company_id')->orderBy('first_name');

	}


	public function getVisitorsTotalAttribute(){

		return '('. $this->visitors()->count() .') Visitantes';

	}


	public function vehicles(){

		return $this->hasMany(Vehicle::class, 'company_id')->orderBy('brand')->orderBy('model')->orderBy('year', 'desc');

	}


	public function getVehiclesTotalAttribute(){

		return '('. $this->vehicles()->count() .') Vehículos';

	}


}
