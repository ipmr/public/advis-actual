<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Date;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Date::class, function (Faker $faker) {

    $start_at = Carbon::now()->addDays(rand(0, 6));

    return [

        'subject'       => $faker->sentence,
        'description'   => $faker->paragraph,
        'company_id'    => 1,
        'department_id' => 1,
        'host_id'       => 1,
        'start_at'      => $start_at,
        'vigency'       => $faker->randomElement($array = array ('1', '2', '5', '8', '0')),

    ];
});
