<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Employee;
use Faker\Generator as Faker;

$factory->define(Employee::class, function (Faker $faker) {
    return [

        'department_id' => 1,
        'first_name'    => $faker->firstName,
        'last_name'     => $faker->lastName,
        'email'         => $faker->email,
        'jobtitle'      => $faker->jobTitle,
        'phone'         => $faker->tollFreePhoneNumber,
        'ext'           => rand(100, 500),
        'mobile'        => $faker->tollFreePhoneNumber,
        'gender_id'		=> rand(0,1),
        'status_id'     => 0,
        'employee_id' 	=> hexdec(uniqid())

    ];
});
