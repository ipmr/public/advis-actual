<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\MeetingRoom;
use Faker\Generator as Faker;

$factory->define(MeetingRoom::class, function (Faker $faker) {
    return [

        'name'        => $faker->word,
        'description' => $faker->sentence,
        'location'    => $faker->sentence,

    ];
});
