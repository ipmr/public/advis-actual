<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DateVisitor;
use Faker\Generator as Faker;

$factory->define(DateVisitor::class, function (Faker $faker) {
    return [
        'date_id'    => 1,
        'company_id' => 1,
        'visitor_id' => 1,
        'date_visitor_id' => hexdec(uniqid())
    ];
});
