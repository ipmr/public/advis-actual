<?php

use App\Config;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Config::class, 1)->create();

        factory(User::class, 1)->create([

            'first_name' => 'System',
            'last_name'  => 'Admin',
            'email'      => 'admin@admin.com',
            'role'       => 'admin',
            'password'   => \Hash::make('password')

        ]);

    }
}
