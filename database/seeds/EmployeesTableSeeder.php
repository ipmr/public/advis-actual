<?php

use App\Employee;
use App\EmployeeHistory;
use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	factory(Employee::class, 30)->create()->each(function($employee){

    		$employee->update([

    			'department_id' => rand(1,5)

    		]);

            factory(EmployeeHistory::class, 5)->create([

                'employee_id' => $employee->id
                
            ]);

    	});

    }
}
